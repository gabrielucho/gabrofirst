package br.com.gabro.first.scheduler;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.business.IWorklogBusiness;
import br.com.gabro.first.util.TimeUtil;

@Named
@ApplicationScoped
public class WorklogScheduler {

	@Inject
	private IWorklogBusiness worklogBusiness;

	@Inject
	private IUserBusiness userBusiness;

	private Scheduler scheduler;
	
	public WorklogScheduler() {

	}

	public void schedule() {
		JobDetail job = JobBuilder.newJob(WorklogLauncherJob.class).withIdentity("timeJob", "timelauncher").build();
		job.getJobDataMap().put("worklogBusiness", worklogBusiness);
		job.getJobDataMap().put("userBusiness", userBusiness);
		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("timeTrigger", "timelauncher").withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(1).repeatForever()).build();
		try {
			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			TimeUtil.handleError(e);
		}
	}
	
	public void finish() {
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			TimeUtil.handleError(e);
		}
	}

}
