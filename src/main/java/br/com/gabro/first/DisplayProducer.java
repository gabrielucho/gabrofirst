package br.com.gabro.first;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.eclipse.swt.widgets.Display;

@ApplicationScoped
public class DisplayProducer {

	private Display display;

	public DisplayProducer() {
		display = Display.getDefault();
	}

	@Produces
	@Named("defaultDisplay")
	public Display getDisplay() {
		return display;
	}

}
