package br.com.gabro.first.business;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import br.com.gabro.first.entity.Project;

@ApplicationScoped
public interface IProjectBusiness extends ITimeLauncherBusiness<Project> {

	List<Project> listAllProjects();

	void delete(Project project);

}
