/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.business;

import java.util.List;

import br.com.gabro.first.entity.IEntity;
import br.com.gabro.first.entity.User;

public interface ITimeLauncherBusiness<T extends IEntity> {

	T getById(Class<T> clazz, Long id);

	void save(T entity);

	void delete(T entity);

	List<T> searchByHql(String jpql);

	List<T> listAll(User user);

}
