/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.screen.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import br.com.gabro.first.ChronometerEntries;
import br.com.gabro.first.business.IActivityBusiness;
import br.com.gabro.first.business.IProjectBusiness;
import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.entity.Activity;
import br.com.gabro.first.entity.Project;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.screen.ITimeLauncherCrudScreen;
import br.com.gabro.first.screen.component.CrudButtons;
import br.com.gabro.first.util.Props;
import br.com.gabro.first.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 20/11/2012 22:43:45
 */
@Named
public class ActivityScreen extends AbstractNoResizableScreen implements ITimeLauncherCrudScreen {

  @Inject
  @Named("defaultDisplay")
  private static Display display;

  @Inject
  private ChronometerEntries chronometerEntries;

  @Inject
  private IActivityBusiness business;

  @Inject
  private IProjectBusiness projectBusiness;

  @Inject
  private IUserBusiness userBusiness;

  @Inject
  private Props prp;

  @Inject
  @Named("currentUser")
  private User user;

  private List<Project> projects;
  private Activity oldActivity;

  private Combo cmbProject;
  private Text txName;
  private Table tbActivities;
  private Button btnActiveYes;
  private Button btnActiveNo;
  private CrudButtons buttons;

  private TableColumn tbClItem;
  private TableColumn tbClProject;
  private TableColumn tbClName;
  private TableColumn tbClActive;

  /**
   * @param display
   * @param width
   * @param height
   */
  public ActivityScreen() {
	super();
	setSize(new Point(354, 500));
	super.composeWindowBase();
	prp = new Props();
	oldActivity = null;
	
	setText(prp.get("activity.title"));
	lblScreenTitle.setText(prp.get("activity.title"));

	cmbProject = new Combo(composite, SWT.NONE);
	cmbProject.setBounds(99, 70, 231, 28);

	Label lblName = new Label(composite, SWT.NONE);
	lblName.setBounds(10, 115, 85, 16);
	lblName.setText(prp.get("activity.name"));
	lblName.setFont(fieldFont);

	txName = new Text(composite, SWT.BORDER);
	txName.setToolTipText(prp.get("activity.tooltip.name"));
	txName.setTextLimit(50);
	txName.setBounds(100, 111, 231, 26);
	txName.setFocus();
	txName.setFont(fieldFont);

	Label lblActive = new Label(composite, SWT.NONE);
	lblActive.setBounds(11, 155, 85, 16);
	lblActive.setText(prp.get("activity.active"));

	btnActiveYes = new Button(composite, SWT.RADIO);
	btnActiveYes.setBounds(103, 154, 62, 22);
	btnActiveYes.setText(prp.get("label.yes"));
	btnActiveYes.setSelection(Boolean.TRUE);
	btnActiveYes.addSelectionListener(new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		btnActiveYes.setSelection(Boolean.TRUE);
		btnActiveNo.setSelection(Boolean.FALSE);
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
	  }
	});

	btnActiveNo = new Button(composite, SWT.RADIO);
	btnActiveNo.setBounds(175, 154, 62, 22);
	btnActiveNo.setText(prp.get("label.no"));
	btnActiveNo.addSelectionListener(new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		btnActiveNo.setSelection(Boolean.TRUE);
		btnActiveYes.setSelection(Boolean.FALSE);
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
	  }
	});

	Label lblSep1 = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
	lblSep1.setBounds(9, 223, 322, 2);

	tbActivities = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
	tbActivities.setBounds(9, 231, 322, 225);
	tbActivities.setHeaderVisible(true);
	tbActivities.setLinesVisible(true);

	// tbActivities.addSelectionListener(deleteSelection);

	tbClItem = new TableColumn(tbActivities, SWT.NONE);
	tbClItem.setWidth(30);
	tbClItem.setText("#");

	tbClProject = new TableColumn(tbActivities, SWT.NONE);
	tbClProject.setWidth(64);
	tbClProject.setText(prp.get("activity.project.acronym"));

	tbClName = new TableColumn(tbActivities, SWT.NONE);
	tbClName.setWidth(175);
	tbClName.setText(prp.get("activity.name"));

	tbClActive = new TableColumn(tbActivities, SWT.NONE);
	tbClActive.setWidth(50);
	tbClActive.setText(prp.get("activity.active"));

	Label lblProjeto = new Label(composite, SWT.NONE);
	lblProjeto.setBounds(10, 75, 85, 16);
	lblProjeto.setText(prp.get("activity.project"));

	buttons = new CrudButtons(composite, prp);
	buttons.setLocation(21, 182);
	setCrudActions(buttons);

	cmbProject.addModifyListener(new ModifyListener() {
	  public void modifyText(ModifyEvent arg0) {
		Project project = null;
		int projIndex = (cmbProject.getSelectionIndex() >= 0) ? cmbProject.getSelectionIndex() : 0;
		project = projects.get(projIndex);
		loadActivities(project);
		txName.setText("");
		btnActiveNo.setSelection(Boolean.FALSE);
		btnActiveYes.setSelection(Boolean.TRUE);
	  }
	});

	tbActivities.addSelectionListener(new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		try {
		  oldActivity = (Activity) tbActivities.getSelection()[0].getData();
		} catch (Exception e) {
		  TimeUtil.handleError(e);
		  oldActivity = null;
		}
		if (oldActivity != null) {
		  if (oldActivity.getProject() != null) {
			int i = 0;
			for (Project prj : projects) {
			  if (prj.getId().equals(oldActivity.getProject().getId())) {
				cmbProject.select(i);
			  }
			  i++;
			}
		  }
		  txName.setText(oldActivity.getName());
		  if (oldActivity.isActive()) {
			btnActiveYes.setSelection(Boolean.TRUE);
			btnActiveNo.setSelection(Boolean.FALSE);
		  } else {
			btnActiveNo.setSelection(Boolean.TRUE);
			btnActiveYes.setSelection(Boolean.FALSE);
		  }
		}
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
	  }
	});

	addSaveModifyListenerToWidgets(buttons, new Widget[] { txName, cmbProject });
	addSaveSelectionListenerToWidgets(buttons, new Widget[] { btnActiveYes, btnActiveNo });

	addDeleteModifyListenerToWidgets(buttons, new Widget[] { cmbProject });
	addDeleteSelectionListenerToWidgets(buttons, new Widget[] { tbActivities });
  }

  public void initScreen() {
	oldActivity = null;
	projects = projectBusiness.listAll(user);
	loadComboProject();
	tbActivities.removeAll();
	Project projectSelected = projects.get(cmbProject.getSelectionIndex());
	loadActivities(projectSelected);
  }

  public boolean isEnabledSave() {
	return ((cmbProject.getText() != null && cmbProject.getText().length() > 0) && (txName.getText().length() > 0) && (btnActiveNo.getSelection() || btnActiveYes.getSelection()));
  }

  public boolean isEnabledDelete() {
	return (oldActivity != null);
  }

  public void saveAction() {
	Activity activity = new Activity();
	if (oldActivity != null) {
	  activity.setId(oldActivity.getId());
	}
	activity.setName(txName.getText());
	activity.setUser(user);
	if (btnActiveNo.getSelection()) {
	  activity.setActive(Boolean.FALSE);
	} else {
	  activity.setActive(Boolean.TRUE);
	}
	Project selectedProject = projects.get(cmbProject.getSelectionIndex());
	Project project = projectBusiness.getById(Project.class, selectedProject.getId());
	activity.setProject(project);
	user = userBusiness.getCurrentUser();
	activity.setUser(user);
	business.save(activity);

	chronometerEntries.loadMenuEntries();

	clearAction();
	loadActivities(project);
  }

  public void clearAction() {
	oldActivity = null;
	txName.setText("");
	btnActiveYes.setSelection(Boolean.TRUE);
	btnActiveNo.setSelection(Boolean.FALSE);
	// cmbProject.select(0);
  }

  public void deleteAction() {
	if (oldActivity != null) {
	  Project project = oldActivity.getProject();
	  business.delete(oldActivity);
	  clearAction();
	  loadActivities(project);
	}
  }

  private void loadComboProject() {
	try {
	  cmbProject.removeAll();
	  String[] projs = new String[projects.size()];
	  int selected = 0;
	  int counter = 0;
	  for (Project pr : projects) {
		projs[counter] = pr.getName();
		if (oldActivity != null && oldActivity.getProject().getName().equalsIgnoreCase(pr.getName())) {
		  selected = counter;
		}
		counter++;
	  }
	  cmbProject.setItems(projs);
	  cmbProject.select(selected);
	} catch (Exception e) {
	  e.printStackTrace();
	}
  }

  private void loadActivities(Project project) {
	try {
	  // oldActivity = null;
	  tbActivities.removeAll();
	  List<Activity> activities = business.listAll(user);
	  Integer counter = 1;
	  for (Activity a : activities) {
		TableItem tbItem = new TableItem(tbActivities, SWT.NONE);
		String active;
		active = a.isActive() ? prp.get("label.yes") : prp.get("label.no");
		tbItem.setText(new String[] { Integer.toString(counter), a.getProject().getAcronym(), a.getName(), active });
		tbItem.setData(a);
		counter++;
	  }
	} catch (Exception e) {
	  TimeUtil.handleError(e);
	}
  }
}
