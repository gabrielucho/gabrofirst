package br.inf.efaber;

import java.util.Calendar;
import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import br.com.gabro.first.util.TimeUtil;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

  /**
   * Create the test case
   * @param testName 
   * name of the test case
   */
  public AppTest(String testName) {
	super(testName);
  }
  
  public static void main(String[] args) {
	Calendar cal = Calendar.getInstance();
	cal.set(Calendar.HOUR_OF_DAY, 4);
	cal.set(Calendar.MINUTE, 32);
	Date dt1 = cal.getTime();
	
	cal.set(Calendar.HOUR_OF_DAY, 7);
	cal.set(Calendar.MINUTE, 1);
	Date dt2 = cal.getTime();
	
	Integer[] interval = TimeUtil.getHoursAndMinutesBetweenDates(dt2, dt1);
	
	System.out.println(interval[0] + " " + interval[1]);
	
  }
  
  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
	return new TestSuite(AppTest.class);
  }

  /**
   * Rigourous Test :-)
   */
  public void testApp() {
	assertTrue(true);
  }
}
