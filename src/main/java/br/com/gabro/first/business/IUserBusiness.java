package br.com.gabro.first.business;

import javax.enterprise.context.ApplicationScoped;

import br.com.gabro.first.entity.User;

@ApplicationScoped
public interface IUserBusiness extends ITimeLauncherBusiness<User> {

	User getCurrentUser();

}
