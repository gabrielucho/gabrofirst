package br.com.gabro.first.screen;

public interface ITimeLauncherCrudScreen {

	/**
	 * Utilizado para inicializar os componentes da tela
	 */
	public void initScreen();

	/**
	 * Método utilizado para limpar o conteúdo da tela. Utilizado após operações
	 * CRUD. Utilizado em conjunto com o método initScreen().
	 */
	public void clearAction();

	/**
	 * Método invocado para avaliar se o conteúdo da tela está pronto para ser
	 * salvo. Controla a habilitação do botão Salvar. Retorna True se o botão
	 * Salvar pode ser habilitado. Do contrário, retorna False.
	 * 
	 * @return boolean
	 */
	public boolean isEnabledSave();

	/**
	 * Método invocado para avaliar se o conteúdo da tela está pronto para ser
	 * excluído. Controla a habilitação do botão Delete. Retorna True caso o
	 * botão Delete pode ser habilitado. Retorna False caso contrário.
	 * 
	 * @return boolean
	 */
	public boolean isEnabledDelete();

	/**
	 * Método utilizado para efetuar a operação de persistência de dados. O
	 * botão Salvar deve invocar este método.
	 */
	public void saveAction();

	/**
	 * Método utilizado para efetuar a operação de exclusão de dados. O botão
	 * Delete deve invocar este método.
	 */
	public void deleteAction();

	/**
	 * Método utilizado para cancelar uma operação. Utilizado para fechar uma
	 * janela. Deve ser invocado pelo botão Cancelar.
	 */
	public void cancelAction();

}
