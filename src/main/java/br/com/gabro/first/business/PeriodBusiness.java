package br.com.gabro.first.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Query;

import br.com.gabro.first.entity.Period;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.util.TimeUtil;

@ApplicationScoped
public class PeriodBusiness extends AbstractTimeLauncherBusiness<Period> implements IPeriodBusiness {

	public PeriodBusiness() {
		super();
	}

	public List<Period> listAll(User user) {
		List<Period> results = null;
		Query query = entityManager.createQuery("SELECT p FROM Period p WHERE p.user.id = :idUser ORDER BY p.startTime");
		query.setParameter("idUser", user.getId());
		try {
			results = (List<Period>) query.getResultList();
		} catch (Exception e) {
			results = new ArrayList<Period>(0);
		}
		return results;
	}

	public List<Period> getPeriodsByDate(User user, Date date) {
		List<Period> results = null;
		Query query = entityManager.createQuery("SELECT p FROM Period p WHERE p.user.id = :idUser AND p.date = :datePeriod ORDER BY p.startTime");
		query.setParameter("idUser", user.getId());
		query.setParameter("datePeriod", date);
		try {
			results = (List<Period>) query.getResultList();
		} catch (Exception e) {
			results = new ArrayList<Period>(0);
		}
		return results;
	}

	@Override
	public void save(Period entity) {

		if (entity.getEndTime() != null) {
			int minutes = TimeUtil.getElapsedMinutesBetweenDates(entity.getEndTime(), entity.getStartTime());
			entity.setTotalMinutes(minutes);
		} else {
			entity.setTotalMinutes(0);
		}
		super.save(entity);
	}

	public Integer getTotalMinutesWorkedByDate(User user, Date date) {
		Query query = entityManager.createQuery("SELECT SUM(p.totalMinutes) FROM Period p WHERE p.user.id = :idUser AND p.date = :datePeriod");
		query.setParameter("idUser", user.getId());
		query.setParameter("datePeriod", date);
		Long result;
		try {
			result = (Long) query.getSingleResult();
			if (result == null) {
				result = 0L;
			}
		} catch (Exception e) {
			result = 0L;
			TimeUtil.handleError(e);
		}
		return result.intValue();
	}

}
