package br.com.gabro.first.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

/**
 * @author leopoldo.barreiro
 * @since 17/11/2012 11:05:34
 */
public class TimeUtil {

	public static final String DATETIME_MASK = "yyyy-MM-dd HH:mm";
	public static final String DATE_MASK = "yyyy-MM-dd";
	public static final String HH_MM = "hh:mm";

	private static Calendar cal;
	private static SimpleDateFormat sdf;

	@Inject
	private static Props prp;

	public TimeUtil() {
	}

	/**
	 * Retorna a Data atual do sistema, com a máscara correta segundo o idioma
	 * configurado no aplicativo.
	 * 
	 * @return actualDate
	 */
	public static String actualDateString(String maskDate) {
		cal = Calendar.getInstance();
		String actualDate = dateToString(cal.getTime(), maskDate);
		return actualDate;
	}

	/**
	 * Retorna a hora atual do sistema, no formato HH:mm.
	 * 
	 * @return horaAtual
	 */
	public static String getActualTime() {
		cal = Calendar.getInstance();
		String hhmm = extractTimeToDateTime(cal.getTime());
		return hhmm;
	}

	/**
	 * Recebe uma string de representação de uma data. Retorna o objeto Date
	 * resultante do parser.
	 * 
	 * @param str
	 * @return date
	 */
	public static Date stringToDate(String str, String maskDate) {
		sdf = new SimpleDateFormat(maskDate);
		try {
			return sdf.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Retorna uma string com a representação da data passada por parametro.
	 * Utiliza a property "mask.date" como máscara para montar o texto de
	 * representação de data
	 * 
	 * @param dt
	 * @return label Date
	 */
	public static String dateToString(Date dt, String maskDate) {
		sdf = new SimpleDateFormat(maskDate);
		return sdf.format(dt);
	}

	/**
	 * Retorna as Horas e minutos de uma Data, formatando com a máscara HH:mm
	 * 
	 * @param dateTime
	 * @return Label HH:mm
	 */
	public static String extractTimeToDateTime(Date dateTime) {
		StringBuilder sb = new StringBuilder();
		cal = Calendar.getInstance();
		try {
			cal.setTime(dateTime);
			if (cal.get(Calendar.HOUR_OF_DAY) < 10) {
				sb.append("0");
			}
			sb.append(cal.get(Calendar.HOUR_OF_DAY));
			sb.append(":");
			if (cal.get(Calendar.MINUTE) < 10) {
				sb.append("0");
			}
			sb.append(cal.get(Calendar.MINUTE));
		} catch (Exception e) {
			return null;
		}
		return sb.toString();
	}

	/**
	 * Compara datas ao estilo compareTo. Primeiramente retira as informações de
	 * Horas, minutos e segundos das datas.
	 * 
	 * @param dt1
	 * @param dt2
	 * @return -1 se dt1 é maior, 0 se ambas as datas são iguais, 1 se dt2 é
	 *         maior.
	 */
	public static int compareDates(Date dt1, Date dt2) {
		return planDate(dt1).compareTo(planDate(dt2));
	}

	/**
	 * Manipula a data, resetando a informação de horas, minutos e segundos.
	 * 
	 * @param date
	 * @return planDate
	 */
	public static Date planDate(Date date) {
		cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

	/**
	 * Calcula o total de horas e minutos transcorridos entre duas datas.
	 * 
	 * @param dt1
	 *            (data mais recente)
	 * @param dt2
	 *            (data mais antiga)
	 * @return Integer[] [hours, minutes]
	 */
	public static Integer[] getHoursAndMinutesBetweenDates(Date dt1, Date dt2) {
		Integer[] hoursMinutes = new Integer[2];
		try {
			Integer totalMinutes = TimeUtil.getElapsedMinutesBetweenDates(dt1, dt2);
			int hours = (int) Math.floor(totalMinutes / 60);
			int minutes = totalMinutes % 60;
			hoursMinutes[0] = hours;
			hoursMinutes[1] = minutes;
		} catch (Exception e) {
			TimeUtil.handleError(e);
		}
		return hoursMinutes;
	}

	/**
	 * Retorna o total de minutos transcorridos entre duas datas O primeiro
	 * parametro é a data posterior. O segundo parametro é a data mais antiga
	 * 
	 * @param dt1
	 * @param dt2
	 * @return totalMinutes
	 */
	public static Integer getElapsedMinutesBetweenDates(Date dt1, Date dt2) {
		long miliseconds = (dt1.getTime() - dt2.getTime());
		Integer totalMinutes = (int) ((miliseconds / 60) / 1000);
		return totalMinutes;
	}

	/**
	 * @param totalElapsedMinutes
	 * @return Integer[] hoursAndMinutes
	 */
	public static Integer[] calculateHoursAndMinutesFromTotalMinutes(int totalElapsedMinutes) {
		Integer[] hoursAndMinutes = new Integer[2];
		try {
			int hours = (int) Math.floor(totalElapsedMinutes / 60);
			int minutes = totalElapsedMinutes % 60;
			hoursAndMinutes[0] = hours;
			hoursAndMinutes[1] = minutes;
		} catch (Exception e) {
			TimeUtil.handleError(e);
		}
		return hoursAndMinutes;
	}

	public static String getLabelOfElapsedTime(Integer[] hoursAndMinutes) {
		try {
			StringBuilder sb = new StringBuilder();
			if (hoursAndMinutes[0] < 10) {
				sb.append("0");
			}
			sb.append(hoursAndMinutes[0]);
			sb.append(":");
			if (hoursAndMinutes[1] < 10) {
				sb.append("0");
			}
			sb.append(hoursAndMinutes[1]);
			return sb.toString();
		} catch (Exception e) {
			TimeUtil.handleError(e);
			return "00:00";
		}
	}

	/**
	 * Retorna a abreviatura do dia da semana internacionalizado Utiliza as
	 * properties de idiomas do aplicativo
	 * 
	 * @param date
	 * @return dayOfWeek
	 */
	public static String dayOfWeek(Date date, Props prp) {
		cal = Calendar.getInstance();
		cal.setTime(date);
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		String key = "";
		if (dayOfWeek == Calendar.SUNDAY) {
			key = prp.get("weekday.sunday");
		} else if (dayOfWeek == Calendar.MONDAY) {
			key = prp.get("weekday.monday");
		} else if (dayOfWeek == Calendar.TUESDAY) {
			key = prp.get("weekday.tuesday");
		} else if (dayOfWeek == Calendar.WEDNESDAY) {
			key = prp.get("weekday.wednesday");
		} else if (dayOfWeek == Calendar.THURSDAY) {
			key = prp.get("weekday.thursday");
		} else if (dayOfWeek == Calendar.FRIDAY) {
			key = prp.get("weekday.friday");
		} else if (dayOfWeek == Calendar.SATURDAY) {
			key = prp.get("weekday.saturday");
		}
		return key;
	}

	/**
	 * Calcula um dia anterior a data informada
	 * 
	 * @param dt
	 * @return dateBefore
	 */
	public static Date oneDayBefore(Date dt) {
		cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		return cal.getTime();
	}

	/**
	 * Calcula um dia posterior a data informada
	 * 
	 * @param dt
	 * @return dateAfter
	 */
	public static Date oneDayAfter(Date dt) {
		cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/**
	 * Lança uma mensagem de informação
	 * 
	 * @param message
	 */
	public static void handleInfo(String message) {
		System.out.println(message);
	}

	/**
	 * Lança uma mensagem de aviso
	 * 
	 * @param warning
	 */
	public static void handleWarning(String warning) {
		System.out.println(warning);
	}

	/**
	 * Lança uma mensagem de erro
	 * 
	 * @param error
	 */
	public static void handleError(String error) {
		System.out.println(error);
	}

	/**
	 * Realiza o tratamento da excessao para exibicao
	 * 
	 * @param e
	 */
	public static void handleError(Exception e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
	}

}
