/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Text;

/**
 * @author leopoldo.barreiro
 * @since 17/11/2012 12:23:13
 */
public class MaskVerifyListener implements VerifyListener {

  private static final String SLASH = "/";
  private static final String TWOPOINTS = ":";
  private static final int BACKSPACE = 8;
  private static final int DELETE = 127;
  private static final int SLASH_KEYCODE = 47;
  private static final int TWOPOINTS_KEYCODE = 59;
  private List<Integer> numberKeyCodes = new ArrayList<Integer>(0);
  private List<String> numberSymbols = new ArrayList<String>(0);
  private String mask;

  public MaskVerifyListener(String mask) {
	this.mask = mask;
	numberKeyCodes = Arrays.asList(new Integer[] { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57 });
	numberSymbols = Arrays.asList(new String[] { "d", "m", "y", "a", "h" });
  }

  public void verifyText(VerifyEvent e) {
	e.doit = isAllowedChar(e);
  }

  private boolean isAllowedChar(VerifyEvent e) {
	if (e.keyCode == BACKSPACE || e.keyCode == DELETE) {
	  return true;
	}
	String nextAllowed = nextTypeCharAllowed(e);
	if (nextAllowed.equals(SLASH) && e.keyCode == SLASH_KEYCODE) {
	  return true;
	} else if (nextAllowed.equals(TWOPOINTS) && e.keyCode == TWOPOINTS_KEYCODE) {
	  return true;
	} else if (numberSymbols.contains(nextAllowed.toLowerCase()) && numberKeyCodes.contains(e.keyCode)) {
	  return true;
	}
	return false;
  }

  private String nextTypeCharAllowed(VerifyEvent e) {
	Text obj = (Text) e.widget;
	int typedTextLength = obj.getText().length();
	if (typedTextLength >= this.mask.length()) {
	  return "";
	} else {
	  String next = this.mask.substring(typedTextLength, (typedTextLength + 1));
	  return next;
	}
  }
  
}
