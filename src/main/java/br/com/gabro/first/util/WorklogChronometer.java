/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import br.com.gabro.first.entity.Worklog;

/**
 * @author leopoldo.barreiro
 * @since 06/12/2012 21:51:43
 */
public class WorklogChronometer implements Serializable {

  private static final long serialVersionUID = -2604792047934651169L;

  private int interval;
  private Date started;
  private Worklog worklog;
  
  public WorklogChronometer(Worklog worklog, int interval) {
	this.worklog = worklog;
	this.interval = interval;
	this.started = Calendar.getInstance().getTime();
  }

  public int getInterval() {
    return interval;
  }

  public void setInterval(int interval) {
    this.interval = interval;
  }
  
  public Date getStarted() {
    return started;
  }

  public void setStarted(Date started) {
    this.started = started;
  }

  public Worklog getWorklog() {
    return worklog;
  }

  public void setWorklog(Worklog worklog) {
    this.worklog = worklog;
  }
  
}
