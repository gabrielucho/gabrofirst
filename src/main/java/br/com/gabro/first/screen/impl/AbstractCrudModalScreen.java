/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.screen.impl;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.screen.ITimeLauncherCrudScreen;
import br.com.gabro.first.screen.component.CrudButtons;
import br.com.gabro.first.screen.component.CrudButtons.ButtonType;

/**
 * @author leopoldo.barreiro
 * @since 11/11/2012 16:52:38
 */
@Deprecated
public abstract class AbstractCrudModalScreen extends Shell implements ITimeLauncherCrudScreen {

  public static final String MAIN_FONT_FAMILY = "Ubuntu";
  public static final String TITLE_FONT = "Ubuntu";
  public static final int TITLE_FONT_SIZE = 14;
  public static final String MODAL_LOGO = "/img/miniefaber.png";
  
  protected IUserBusiness userBusiness;

  protected Composite composite;
  protected Label lblScreenTitle;
  protected Label lblLogotipo;

  public AbstractCrudModalScreen(Display display) {
	super(display, SWT.APPLICATION_MODAL);
  }

  public AbstractCrudModalScreen(Display display, Point modalSize) {
	super(display, SWT.APPLICATION_MODAL);
	setSize(modalSize);
  }

  public AbstractCrudModalScreen(Display display, int width, int height) {
	super(display, SWT.APPLICATION_MODAL);
	setSize(width, height);
  }

  protected final void composeBase() {
	composite = new Composite(this, SWT.BORDER);
	lblScreenTitle = new Label(composite, SWT.NONE);
	lblLogotipo = new Label(composite, SWT.NONE);

	//	prp.loadProperties();

	int width = getSize().x;
	int height = getSize().y;

	composite.setBounds(8, 8, (width - 16), (height - 16));

	lblScreenTitle.setFont(new Font(this.getDisplay(), TITLE_FONT, TITLE_FONT_SIZE, SWT.BOLD));
	lblScreenTitle.setBounds(20, 10, (width - 110), 30);

	Image imgLogo = new Image(this.getDisplay(), AbstractCrudModalScreen.class.getResourceAsStream(MODAL_LOGO));
	lblLogotipo.setImage(imgLogo);
	lblLogotipo.setBounds((width - 80), 6, 50, 51);

  }

  public final void cancelAction() {
	clearAction();
	setVisible(Boolean.FALSE);
  }

  @Override
  protected void checkSubclass() {
	// Disable the check that prevents subclassing of SWT components
  }

  protected void setCrudActions(CrudButtons buttons) {
	buttons.addControl(ButtonType.SAVE, new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		saveAction();
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		saveAction();
	  }
	});
	buttons.addControl(ButtonType.CLEAR, new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		clearAction();
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		clearAction();
	  }
	});
	buttons.addControl(ButtonType.DELETE, new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		deleteAction();
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		deleteAction();
	  }
	});
	buttons.addControl(ButtonType.CLOSE, new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		cancelAction();
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		cancelAction();
	  }
	});
  }

  protected void addSaveModifyListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
	ModifyListener saveEnableModify = new ModifyListener() {
	  public void modifyText(ModifyEvent arg0) {
		buttons.setEnableSave(isEnabledSave());
	  }
	};
	for (int i = 0; i < widgets.length; i++) {
	  if (widgets[i].getClass().equals(Text.class)) {
		((Text) widgets[i]).addModifyListener(saveEnableModify);
	  } else if (widgets[i].getClass().equals(Combo.class)) {
		((Combo) widgets[i]).addModifyListener(saveEnableModify);
	  }
	}
  }

  protected void addSaveSelectionListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
	SelectionListener saveEnableSelection = new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		buttons.setEnableSave(isEnabledSave());
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		buttons.setEnableSave(isEnabledSave());
	  }
	};

	for (int i = 0; i < widgets.length; i++) {
	  if (widgets[i].getClass().equals(Button.class)) {
		((Button) widgets[i]).addSelectionListener(saveEnableSelection);
	  }
	}
  }

  protected void addDeleteModifyListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
	ModifyListener deleteEnableModify = new ModifyListener() {
	  public void modifyText(ModifyEvent arg0) {
		buttons.setEnableDelete(isEnabledSave());
	  }
	};
	for (int i = 0; i < widgets.length; i++) {
	  if (widgets[i].getClass().equals(Text.class)) {
		((Text) widgets[i]).addModifyListener(deleteEnableModify);
	  } else if (widgets[i].getClass().equals(Combo.class)) {
		((Combo) widgets[i]).addModifyListener(deleteEnableModify);
	  }
	}
  }
  
  protected void addDeleteSelectionListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
	SelectionListener deleteEnableSelection = new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		buttons.setEnableDelete(isEnabledDelete());
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		buttons.setEnableDelete(isEnabledDelete());
	  }
	};

	for (int i = 0; i < widgets.length; i++) {
	  if (widgets[i].getClass().equals(Table.class)) {
		((Table) widgets[i]).addSelectionListener(deleteEnableSelection);
	  } else if (widgets[i].getClass().equals(Button.class)) {
		((Button) widgets[i]).addSelectionListener(deleteEnableSelection);
	  }
	}
  }

}
