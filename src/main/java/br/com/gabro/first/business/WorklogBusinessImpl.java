/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Query;

import br.com.gabro.first.ChronometerEntries;
import br.com.gabro.first.entity.Activity;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.entity.Worklog;
import br.com.gabro.first.util.Props;
import br.com.gabro.first.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 18/11/2012 18:33:20
 */
@ApplicationScoped
public class WorklogBusinessImpl extends AbstractTimeLauncherBusiness<Worklog> implements IWorklogBusiness {

  @Inject
  private IActivityBusiness activityBusiness;
  
  @Inject
  private ChronometerEntries chronometerEntries;

  @Inject
  private Props prp;
  
  public WorklogBusinessImpl() {
	super();
  }

  @Override
  public void save(Worklog entity) {
	int minutes = 0;
	try {
	  String[] hoursAndMinutes = entity.getLabelHoursMinutes().split(":");
	  minutes = (Integer.valueOf(hoursAndMinutes[0]) * 60);
	  minutes += Integer.valueOf(hoursAndMinutes[1]);
	} catch (Exception e) {
	  TimeUtil.handleError(e);
	}
	entity.setTotalMinutes(minutes);
	super.save(entity);
  }
  
  @SuppressWarnings("unchecked")
  public List<Worklog> getWorklogsByDate(User user, Date date) {
	List<Worklog> results = null;
	Query query = entityManager.createQuery("SELECT wl FROM Worklog wl WHERE wl.user.id = :idUser AND wl.logDate = :dateLog ORDER BY wl.logDate");
	query.setParameter("idUser", user.getId());
	query.setParameter("dateLog", date);
	try {
	  results = query.getResultList();
	} catch (Exception e) {
	  results = new ArrayList<Worklog>(0);
	  TimeUtil.handleError(e);
	}
	return results;
  }

  @SuppressWarnings("unchecked")
  public List<Worklog> listAll(User user) {
	List<Worklog> results = null;
	Query query = entityManager.createQuery("SELECT wl FROM Worklog wl WHERE wl.user.id = :idUser ORDER BY wl.logDate");
	query.setParameter("idUser", user.getId());
	try {
	  results = (List<Worklog>) query.getResultList();
	} catch (Exception e) {
	  TimeUtil.handleError(e);
	  results = new ArrayList<Worklog>(0);
	}
	return results;
  }

  public void updateChronometer(Long idWorklog, int addInterval) {
	//TODO: verificar se a atividade está ativa ainda. Caso não esteja, avisar o usuário
	Worklog worklog = this.getById(Worklog.class, idWorklog);
	int time = (worklog.getTotalMinutes() == null) ? 0 : worklog.getTotalMinutes();
	time = (time + addInterval);
	worklog.setTotalMinutes(time);
	super.save(worklog);
  }

  public Worklog getOpenedWorklog(User user) {
	Worklog worklog = null;
	try {
	  Query query = entityManager.createQuery("SELECT w FROM Worklog w JOIN FETCH w.user WHERE w.opened = true AND w.user.id = :idUser");
	  query.setParameter("idUser", user.getId());
	  query.setMaxResults(1);
	  @SuppressWarnings("unchecked")
	  List<Worklog> listaWorklog = (List<Worklog>) query.getResultList();
	  worklog = (listaWorklog.size() > 0) ? listaWorklog.get(0) : null; 
	} catch (Exception e) {
	  TimeUtil.handleError(e);
	}
	return worklog;
  }

  
public void openWorklog(Activity activity) {
	Worklog worklog = null;
	entityManager.getTransaction().begin();
	entityManager.flush();
	entityManager.getTransaction().commit();
	Activity activityWorked = activityBusiness.getById(Activity.class, activity.getId());
	closeWorklogs(activityWorked.getUser());
	try {
	  Query openQuery = entityManager.createQuery("SELECT w FROM Worklog w JOIN FETCH w.activity WHERE w.activity.id = :idActivity AND w.logDate = :dtLog");
	  openQuery.setParameter("idActivity", activityWorked.getId());
	  openQuery.setParameter("dtLog", Calendar.getInstance().getTime());
	  openQuery.setMaxResults(1);
	  @SuppressWarnings("unchecked")
	  List<Worklog> listWorklog = (List<Worklog>) openQuery.getResultList();
	  worklog = (listWorklog.size() > 0) ? listWorklog.get(0) : null;
	  if (worklog != null) {
		  entityManager.refresh(worklog);  
	  }
	} catch (Exception e) {
	  TimeUtil.handleError(e);
	}

	if (worklog == null) {
	  worklog = new Worklog();
	  worklog.setActivity(activityWorked);
	  worklog.setUser(activityWorked.getUser());
	  worklog.setLogDate(Calendar.getInstance().getTime());
	  worklog.setTotalMinutes(0);
	  worklog.setDescription(prp.get("msg.worklog.opened.by.chronometer"));
	}
	worklog.setOpened(Boolean.TRUE);
	super.save(worklog);
	chronometerEntries.loadMenuEntries();
  }

  public void closeWorklogs(User user) {
	entityManager.getTransaction().begin();
	Query query = entityManager.createQuery("UPDATE Worklog w SET w.opened = false WHERE w.user.id = :idUser");
	query.setParameter("idUser", user.getId());
	query.executeUpdate();
	entityManager.flush();
	entityManager.getTransaction().commit();
	try {
	  chronometerEntries.loadMenuEntries();
	} catch (Exception e) {
	  TimeUtil.handleError(e);
	}
  }
  
}
