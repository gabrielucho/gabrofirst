package br.com.gabro.first.business;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Query;

import br.com.gabro.first.entity.Activity;
import br.com.gabro.first.entity.Project;
import br.com.gabro.first.entity.User;

@ApplicationScoped
public class ActivityBusiness extends AbstractTimeLauncherBusiness<Activity> implements IActivityBusiness {

	public ActivityBusiness() {
		super();
	}

	@SuppressWarnings("unchecked")
	public List<Activity> listAll(User user) {
		Query query = entityManager.createQuery("SELECT ac FROM Activity ac INNER JOIN FETCH ac.project WHERE ac.user.id = :idUser ORDER BY ac.name ASC");
		query.setParameter("idUser", user.getId());
		return (List<Activity>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Activity> listAllByProject(Project project, User user) {
		Query query = entityManager
				.createQuery("SELECT ac FROM Activity ac INNER JOIN FETCH ac.project WHERE ac.project.id = :idProject AND ac.user.id = :idUser ORDER BY ac.name ASC");
		query.setParameter("idProject", project.getId());
		query.setParameter("idUser", user.getId());
		return (List<Activity>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Activity> listAllActivesByProject(Project project, User user) {
		Query query = entityManager
				.createQuery("SELECT ac FROM Activity ac INNER JOIN FETCH ac.project WHERE ac.project.id = :idProject AND ac.active = true AND ac.user.id = :idUser ORDER BY ac.name ASC");
		query.setParameter("idProject", project.getId());
		query.setParameter("idUser", user.getId());
		return (List<Activity>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Activity> listAllActives(User user) {
		Query query = entityManager
				.createQuery("SELECT ac FROM Activity ac INNER JOIN FETCH ac.project WHERE ac.active = true AND ac.user.id = :idUser ORDER BY ac.name ASC");
		query.setParameter("idUser", user.getId());
		return (List<Activity>) query.getResultList();
	}

}
