package br.com.gabro.first.util;

import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import br.com.gabro.first.entity.User;

@Named
@ApplicationScoped
public class Props {

	private static final String PT = "pt";
	private static final String ES_ES = "esES";
	private static final String EN_US = "enUS";
	private static final String PT_BR = "ptBR";
	@SuppressWarnings("unused")
	private static final String EN = "en";
	private static final String ES = "es";
	private Properties properties;

	@Inject
	@Named("currentUser")
	private User user;

	public Props() {
		loadProperties();
	}

	public void loadProperties() {
		if (user != null && StringUtils.isNotEmpty(user.getLanguage())) {
			loadPropertiesContent(user.getLanguage());
		} else {
			Locale locale = Locale.getDefault();
			if (locale.getLanguage().toLowerCase().equals(ES)) {
				loadPropertiesContent(ES_ES);
			} else if (locale.getLanguage().toLowerCase().equals(PT)) {
				loadPropertiesContent(PT_BR);
			} else {
				loadPropertiesContent(EN_US);
			}
		}
	}

	private void loadPropertiesContent(String language) {
		StringBuilder sb = new StringBuilder();
		sb.append("/languages/");
		sb.append(language);
		sb.append(".properties");
		properties = new Properties();
		try {
			properties.load(Props.class.getResourceAsStream(sb.toString()));
		} catch (IOException e) {
			TimeUtil.handleError(e);
		}
	}

	public String get(String key) {
		if (properties.containsKey(key)) {
			return properties.getProperty(key);
		}
		return key;
	}

}
