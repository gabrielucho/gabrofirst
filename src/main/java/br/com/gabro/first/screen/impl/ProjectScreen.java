/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.screen.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import br.com.gabro.first.business.IProjectBusiness;
import br.com.gabro.first.entity.Project;
import br.com.gabro.first.screen.ITimeLauncherCrudScreen;
import br.com.gabro.first.screen.component.CrudButtons;
import br.com.gabro.first.util.Props;

/**
 * @author leopoldo.barreiro
 */
@Named
public class ProjectScreen extends AbstractNoResizableScreen implements ITimeLauncherCrudScreen {

  @Inject
  @Named("defaultDisplay")
  private static Display display;

  @Inject
  private IProjectBusiness projectBusiness;

  @Inject
  private Props prp;

  private Text txAcronym;
  private Text txName;
  private CrudButtons buttons;
  private Project oldProject;
  private Table tbProjects;
  private TableColumn tbClItem;
  private TableColumn tbClName;
  private TableColumn tbClAcronym;
  private Label lblName;
  private Label lblAcronym;

  public ProjectScreen() {
	super();
	setSize(new Point(355, 370));
	super.composeWindowBase();
	oldProject = null;
	prp = new Props();

	setText(prp.get("project.title"));
	lblScreenTitle.setText(prp.get("project.title"));

	lblName = new Label(composite, SWT.NONE);
	lblName.setBounds(12, 68, 90, 16);
	lblName.setText(prp.get("project.name"));

	txName = new Text(composite, SWT.BORDER);
	txName.setBounds(108, 63, 225, 26);
	txName.setTextLimit(50);

	lblAcronym = new Label(composite, SWT.NONE);
	lblAcronym.setBounds(10, 105, 90, 16);
	lblAcronym.setText(prp.get("project.acronym"));

	txAcronym = new Text(composite, SWT.BORDER);
	txAcronym.setBounds(108, 99, 90, 26);
	txAcronym.setTextLimit(5);

	buttons = new CrudButtons(composite, prp);
	buttons.setLocation(23, 145);
	setCrudActions(buttons);

	tbProjects = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
	tbProjects.setBounds(12, 205, 320, 122);
	tbProjects.setHeaderVisible(true);
	tbProjects.setLinesVisible(true);

	tbClItem = new TableColumn(tbProjects, SWT.NONE);
	tbClItem.setWidth(40);
	tbClItem.setText("#");

	tbClAcronym = new TableColumn(tbProjects, SWT.NONE);
	tbClAcronym.setWidth(80);
	tbClAcronym.setText(prp.get("project.acronym"));

	tbClName = new TableColumn(tbProjects, SWT.NONE);
	tbClName.setWidth(195);
	tbClName.setText(prp.get("project.name"));

	addSaveModifyListenerToWidgets(buttons, new Widget[] { txName, txAcronym });
	addDeleteSelectionListenerToWidgets(buttons, new Widget[] { tbProjects });

	tbProjects.addSelectionListener(new SelectionListener() {
	  public void widgetSelected(SelectionEvent e) {
		oldProject = (Project) tbProjects.getSelection()[0].getData();
		txName.setText(oldProject.getName());
		txAcronym.setText(oldProject.getAcronym());
	  }

	  public void widgetDefaultSelected(SelectionEvent e) {
	  }
	});

  }

  public void initScreen() {
	oldProject = null;
	txAcronym.setText("");
	txName.setText("");
	loadProjects();
  }

  public void clearAction() {
	oldProject = null;
	txName.setText("");
	txAcronym.setText("");
	loadProjects();
  }

  public boolean isEnabledSave() {
	return ((txName.getText() != null && txName.getText().length() > 0) && (txAcronym.getText() != null && txAcronym.getText().length() > 0));
  }

  public boolean isEnabledDelete() {
	return (oldProject != null);
  }

  public void saveAction() {
	Project project = new Project();
	if (oldProject != null) {
	  project.setId(oldProject.getId());
	}
	project.setAcronym(txAcronym.getText());
	project.setName(txName.getText());
	projectBusiness.save(project);
	clearAction();
  }

  public void deleteAction() {
	projectBusiness.delete(oldProject);
	clearAction();
  }

  private void loadProjects() {
	tbProjects.removeAll();
	List<Project> projects = projectBusiness.listAllProjects();
	Integer counter = 1;
	for (Project p : projects) {
	  TableItem tbItem = new TableItem(tbProjects, SWT.NONE);
	  tbItem.setText(new String[] { Integer.toString(counter), p.getAcronym(), p.getName() });
	  tbItem.setData(p);
	  counter++;
	}
  }

}
