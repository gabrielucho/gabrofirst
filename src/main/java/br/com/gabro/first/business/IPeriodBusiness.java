package br.com.gabro.first.business;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import br.com.gabro.first.entity.Period;
import br.com.gabro.first.entity.User;

@ApplicationScoped
public interface IPeriodBusiness extends ITimeLauncherBusiness<Period> {

	List<Period> getPeriodsByDate(User user, Date date);

	Integer getTotalMinutesWorkedByDate(User user, Date date);

}
