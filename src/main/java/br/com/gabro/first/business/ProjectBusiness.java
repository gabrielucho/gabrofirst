package br.com.gabro.first.business;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Query;

import br.com.gabro.first.entity.Activity;
import br.com.gabro.first.entity.Project;
import br.com.gabro.first.entity.User;

@ApplicationScoped
public class ProjectBusiness extends AbstractTimeLauncherBusiness<Project> implements IProjectBusiness {

	public ProjectBusiness() {
		super();
	}

	@SuppressWarnings("unchecked")
	public List<Project> listAll(User user) {
		List<Project> projects = new ArrayList<Project>(0);
		Query query = entityManager.createQuery("SELECT p FROM Project p WHERE p.acronym != '' ORDER BY p.name");
		projects = (List<Project>) query.getResultList();
		return projects;
	}

	@SuppressWarnings("unchecked")
	public List<Project> listAllProjects() {
		List<Project> projects = new ArrayList<Project>(0);
		Query query = entityManager.createQuery("SELECT p FROM Project p ORDER BY p.name");
		projects = (List<Project>) query.getResultList();
		return projects;
	}

	@SuppressWarnings("unchecked")
	public void delete(Project project) {
		Query query = entityManager.createQuery("SELECT a FROM Activity a INNER JOIN FETCH a.project WHERE a.project.id = :idProject");
		query.setParameter("idProject", project.getId());
		List<Activity> activities;
		try {
			activities = (List<Activity>) query.getResultList();
		} catch (Exception e) {
			activities = null;
		}
		if (activities == null || activities.isEmpty()) {
			super.delete(project);
		}
	}

}
