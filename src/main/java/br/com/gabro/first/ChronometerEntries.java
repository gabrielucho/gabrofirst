package br.com.gabro.first;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import br.com.gabro.first.business.IActivityBusiness;
import br.com.gabro.first.business.IWorklogBusiness;
import br.com.gabro.first.entity.Activity;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.entity.Worklog;
import br.com.gabro.first.screen.impl.ActivityScreen;
import br.com.gabro.first.screen.impl.PeriodScreen;
import br.com.gabro.first.screen.impl.ProjectScreen;
import br.com.gabro.first.screen.impl.UserScreen;
import br.com.gabro.first.screen.impl.WorkLogScreen;
import br.com.gabro.first.util.Props;

@Named
@ApplicationScoped
public class ChronometerEntries {

	public static final String MENU_ICON_STOP = "/img/stop.png";
	public static final String MENU_ICON_PERIODS = "/img/chronometer.png";
	public static final String MENU_ICON_WORKLOGS = "/img/worklog.png";
	public static final String MENU_ICON_USER_OPTS = "/img/useropt.png";
	public static final String MENU_ICON_PROJECT = "/img/project.png";
	public static final String MENU_ICON_WORKLOG_STOPED = "/img/go.png";
	public static final String MENU_ICON_WORKLOG_RUNNING = "/img/hourglass.png";
	public static final String MENU_ICON_ACTIVITY = "/img/activity.png";
	public static final String MENU_ICON_QUIT = "/img/quit.png";
	public static final String MENU_ITEM_ACTIVITY = "MENU_ITEM_ACTIVITY";
	public static final String MENU_ITEM_SEPARATOR_2 = "MENU_ITEM_SEPARATOR_1";
	public static final String MENU_ITEM_SEPARATOR_1 = "MENU_ITEM_SEPARATOR_1";

	@Inject
	private PeriodScreen periodScreen;

	@Inject
	private ActivityScreen activityScreen;

	@Inject
	private ProjectScreen projectScreen;

	@Inject
	private WorkLogScreen worklogScreen;

	@Inject
	private UserScreen userScreen;

	@Inject
	@Named("defaultDisplay")
	private Display display;

	@Inject
	private IActivityBusiness activityBusiness;

	@Inject
	private IWorklogBusiness worklogBusiness;

	@Inject
	@Named("currentUser")
	private User user;

	@Inject
	private Props prp;

	private Menu menu;

	private List<Activity> activities = new ArrayList<Activity>(0);

	public ChronometerEntries() {
	}

	public void createMenu() {
		menu = new Menu(periodScreen, SWT.PUSH);
	}

	public void loadActivities() {
		activities = activityBusiness.listAllActives(user);
	}

	public void addActivitiesMenuItems() {
		loadActivities();

		Activity startedActivity = null;
		Worklog openedWorklog = worklogBusiness.getOpenedWorklog(user);
		if (openedWorklog != null && openedWorklog.getActivity() != null) {
			startedActivity = openedWorklog.getActivity();
		}

		for (final Activity activity : activities) {
			MenuItem mnActiveActivity = new MenuItem(menu, SWT.PUSH);
			mnActiveActivity.setText(activity.getName());
			if (startedActivity != null && startedActivity.equals(activity)) {
				mnActiveActivity.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_WORKLOG_RUNNING)));
			} else {
				mnActiveActivity.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_WORKLOG_STOPED)));
			}
			mnActiveActivity.setData(activity);
			mnActiveActivity.addSelectionListener(new SelectionListener() {
				public void widgetSelected(SelectionEvent arg0) {
					worklogBusiness.openWorklog(activity);
				}

				public void widgetDefaultSelected(SelectionEvent arg0) {
				}
			});
		}

		if (openedWorklog != null && openedWorklog.getActivity() != null) {
			MenuItem mnPauseChronometer = new MenuItem(menu, SWT.PUSH);
			mnPauseChronometer.setText(prp.get("traymenu.pausechronometer"));
			mnPauseChronometer.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_STOP)));
			mnPauseChronometer.addSelectionListener(new SelectionListener() {
				public void widgetSelected(SelectionEvent arg0) {
					worklogBusiness.closeWorklogs(user);
				}

				public void widgetDefaultSelected(SelectionEvent arg0) {
				}
			});
		}
	}

	public void loadMenuEntries() {
		if (menu == null) {
			return;
		}
		if (menu.isDisposed()) {
			return;
		}
		for (MenuItem mItem : menu.getItems()) {
			mItem.dispose();
		}
		// Time Log Screen
		MenuItem mnTimeLog = new MenuItem(menu, SWT.PUSH);
		// mnTimeLog.setAccelerator(SWT.CTRL+'T');
		mnTimeLog.setText(prp.get("traymenu.timelog"));
		mnTimeLog.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_PERIODS)));
		mnTimeLog.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				periodScreen.open();
				periodScreen.layout();
				periodScreen.initScreen();
			}
		});

		// Work Log Screen
		MenuItem mnWorklog = new MenuItem(menu, SWT.PUSH);
		mnWorklog.setText(prp.get("traymenu.worklog"));
		mnWorklog.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_WORKLOGS)));
		mnWorklog.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				worklogScreen.open();
				worklogScreen.layout();
				worklogScreen.initScreen();
			}
		});

		MenuItem mnUserOpt = new MenuItem(menu, SWT.PUSH);
		mnUserOpt.setText(prp.get("traymenu.useropt"));
		mnUserOpt.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_USER_OPTS)));
		mnUserOpt.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				userScreen.open();
				userScreen.layout();
				userScreen.initScreen();
			}
		});

		@SuppressWarnings("unused")
		MenuItem mnSep0 = new MenuItem(menu, SWT.SEPARATOR);

		// Project
		MenuItem mnProject = new MenuItem(menu, SWT.PUSH);
		mnProject.setText(prp.get("traymenu.project"));
		mnProject.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_PROJECT)));
		mnProject.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				projectScreen.open();
				projectScreen.layout();
				projectScreen.initScreen();
			}
		});

		// Activity
		MenuItem mnActivity = new MenuItem(menu, SWT.PUSH);
		mnActivity.setText(prp.get("traymenu.activity"));
		mnActivity.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_ACTIVITY)));
		mnActivity.setData(new String(MENU_ITEM_ACTIVITY));
		mnActivity.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				activityScreen.open();
				activityScreen.layout();
				activityScreen.initScreen();
			}
		});

		// Separator
		MenuItem mnSep1 = new MenuItem(menu, SWT.SEPARATOR);
		mnSep1.setData(new String(MENU_ITEM_SEPARATOR_1));

		// TODO incluir itens do cronometro
		addActivitiesMenuItems();

		// Separator
		MenuItem mnSep2 = new MenuItem(menu, SWT.SEPARATOR);
		mnSep2.setData(new String(MENU_ITEM_SEPARATOR_2));

		// Quit
		MenuItem mnQuit = new MenuItem(menu, SWT.PUSH);
		mnQuit.setText(prp.get("traymenu.quit"));
		mnQuit.setImage(new Image(display, App.class.getResourceAsStream(MENU_ICON_QUIT)));
		mnQuit.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				menu.dispose();
				// System.exit(0);
			}
		});
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

}
