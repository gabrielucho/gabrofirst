package br.com.gabro.first.business;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.gabro.first.entity.IEntity;
import br.com.gabro.first.util.TimeUtil;

public abstract class AbstractTimeLauncherBusiness<T extends IEntity> implements ITimeLauncherBusiness<T> {

	@Inject
	protected EntityManager entityManager;

	public T getById(Class<T> clazz, Long id) {
		T entity = entityManager.find(clazz, id);
		return entity;
	}

	public void save(T entity) {
		try {
			entityManager.getTransaction().begin();
			if (entity.getId() == null) {
				entityManager.persist(entity);
			} else {
				entityManager.merge(entity);
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			TimeUtil.handleError(e);
		}
	}

	public void delete(T entity) {
		entityManager.getTransaction().begin();
		entityManager.remove(entity);
		entityManager.flush();
		entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<T> searchByHql(String jpql) {
		List<T> results = null;
		Query query = entityManager.createQuery(jpql);
		try {
			results = (List<T>) query.getResultList();
		} catch (Exception e) {
			results = new ArrayList<T>(0);
		}
		return results;
	}

}
