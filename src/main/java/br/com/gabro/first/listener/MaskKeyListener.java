/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.listener;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

/**
 * @author leopoldo.barreiro
 * @since 01/12/2012 22:11:52
 */
public class MaskKeyListener implements KeyListener {

  private static final String SLASH = "/";
  private static final String TWOPOINTS = ":";
  private String mask;
  
  public MaskKeyListener(String mask) {
	this.mask = mask;
  }

  public void keyPressed(KeyEvent e) {
//	evaluateContent(e);	
  }

  public void keyReleased(KeyEvent e) {
	evaluateContent(e);	
  }
  
  private void evaluateContent(KeyEvent e) {
	if (!e.widget.getClass().equals(Text.class)) {
	  return;
	}
	Text field = (Text) e.widget;
	if (field.getText().length() >= this.mask.length()) {
	  return;
	}
	if (field.getText().length() >= field.getTextLimit()) {
	  return;
	}
	String content = field.getText();
	String nextCharExpected = mask.substring(content.length(), (content.length()+1));
	if (nextCharExpected.equals(SLASH)) {
	  field.setText(content + SLASH);
	}
	if (nextCharExpected.equals(TWOPOINTS)) {
	  field.setText(content + TWOPOINTS);
	}
	e.widget = field;
  }

}
