package br.com.gabro.first;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;
import org.jboss.weld.environment.se.events.ContainerInitialized;

import br.com.gabro.first.business.IActivityBusiness;
import br.com.gabro.first.business.IPeriodBusiness;
import br.com.gabro.first.business.IProjectBusiness;
import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.business.IWorklogBusiness;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.scheduler.WorklogLauncherJob;
import br.com.gabro.first.scheduler.WorklogScheduler;
import br.com.gabro.first.screen.impl.ActivityScreen;
import br.com.gabro.first.screen.impl.PeriodScreen;
import br.com.gabro.first.screen.impl.ProjectScreen;
import br.com.gabro.first.screen.impl.UserScreen;
import br.com.gabro.first.screen.impl.WorkLogScreen;
import br.com.gabro.first.util.TimeUtil;

@ApplicationScoped
public class App {

	private static final String TRAYICON_IMAGE = "/img/trayicon32.png";

	@Inject
	@Named("defaultDisplay")
	private Display display;

	@Inject
	private EntityManager entityManager;

	@Inject
	private IUserBusiness userBusiness;

	@Inject
	private IPeriodBusiness periodBusiness;

	@Inject
	private IWorklogBusiness worklogBusiness;

	@Inject
	private IActivityBusiness activityBusiness;

	@Inject
	private IProjectBusiness projectBusiness;

	@Inject
	@Named("currentUser")
	private User user;

	@Inject
	private ActivityScreen activityScreen;

	@Inject
	private ProjectScreen projectScreen;

	@Inject
	private WorkLogScreen workLogScreen;

	@Inject
	private PeriodScreen periodScreen;

	@Inject
	private UserScreen userScreen;

	private Menu menu;

	@SuppressWarnings("unused")
	private WorklogLauncherJob worklogJob;

	@Inject
	private ChronometerEntries chronometerEntries;

	@Inject
	private WorklogScheduler worklogScheduler;

	public void main(@Observes ContainerInitialized event) {
		try {
			worklogBusiness.closeWorklogs(user);
			worklogScheduler.schedule();
			// System Tray
			Tray tray = display.getSystemTray();
			if (tray != null) {
				TrayItem item = new TrayItem(tray, SWT.NONE);
				item.setImage(new Image(display, App.class.getResourceAsStream(TRAYICON_IMAGE)));
				// Application Menu
				chronometerEntries.createMenu();
				chronometerEntries.loadMenuEntries();
				menu = chronometerEntries.getMenu();
				item.addSelectionListener(new SelectionListener() {
					public void widgetSelected(SelectionEvent arg0) {
						menu.setVisible(Boolean.TRUE);
					}

					public void widgetDefaultSelected(SelectionEvent arg0) {
						menu.setVisible(Boolean.TRUE);
					}
				});
				item.addListener(SWT.MenuDetect, new Listener() {
					public void handleEvent(Event event) {
						menu.setVisible(true);
					}
				});
			}
			while (!menu.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
			worklogBusiness.closeWorklogs(user);
			entityManager.close();
			System.exit(0);
		} catch (Exception e) {
			TimeUtil.handleError(e);
			e.printStackTrace();
		}
	}

}
