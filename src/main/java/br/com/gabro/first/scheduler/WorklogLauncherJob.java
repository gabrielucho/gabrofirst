package br.com.gabro.first.scheduler;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.business.IWorklogBusiness;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.entity.Worklog;

@Named
@ApplicationScoped
public class WorklogLauncherJob implements Job {

	private Worklog worklog;

	private int minutes = 1;

	private User user;

	@Inject
	private IWorklogBusiness worklogBusiness;

	@Inject
	private IUserBusiness userBusiness;

	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		worklogBusiness = (IWorklogBusiness) dataMap.get("worklogBusiness");
		userBusiness = (IUserBusiness) dataMap.get("userBusiness");
		user = userBusiness.getCurrentUser();
		worklog = worklogBusiness.getOpenedWorklog(user);
		if (worklog != null && worklog.getId() != null) {
			worklogBusiness.updateChronometer(worklog.getId(), minutes);
		}
	}

}
