/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.screen.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import br.com.gabro.first.ChronometerEntries;
import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.screen.component.CrudButtons;
import br.com.gabro.first.util.Props;

/**
 * @author leopoldo.barreiro
 * @since 23/12/2012 13:58:41
 */
@Named
public class UserScreen extends AbstractNoResizableScreen {

	@Inject
	@Named("currentUser")
	private User user;

	@Inject
	private IUserBusiness userBusiness;

	@Inject
	private ChronometerEntries chronometerEntries;

	@Inject
	private Props prp;

	// private transient @Inject
	// @Named("defaultResourceBundle")
	// ResourceBundle bundle;

	private Text txUsername;

	private Text txEmail;

	private CCombo comboLanguage;

	private CrudButtons buttons;

	private String[] langs;

	/**
	 * @param display
	 */
	public UserScreen() {
		super();
		setSize(new Point(382, 293));
		super.composeWindowBase();
		prp = new Props();

		lblScreenTitle.setText(prp.get("settings.title"));
		setText(prp.get("settings.title"));
		// setText(bundle.getString("settings.title"));

		langs = new String[3];
		langs[0] = new String("ptBR");
		langs[1] = new String("enUS");
		langs[2] = new String("esES");

		Label lbUsername = new Label(composite, SWT.NONE);
		lbUsername.setBounds(24, 84, 85, 16);
		lbUsername.setText(prp.get("settings.user"));

		txUsername = new Text(composite, SWT.BORDER);
		txUsername.setEnabled(false);
		txUsername.setEditable(false);
		txUsername.setBounds(115, 79, 224, 26);

		Label lbEmail = new Label(composite, SWT.NONE);
		lbEmail.setBounds(24, 124, 85, 16);
		lbEmail.setText(prp.get("settings.email"));

		txEmail = new Text(composite, SWT.BORDER);
		txEmail.setBounds(116, 118, 224, 26);

		Label lbLanguage = new Label(composite, SWT.NONE);
		lbLanguage.setBounds(23, 165, 85, 16);
		lbLanguage.setText(prp.get("settings.language"));

		comboLanguage = new CCombo(composite, SWT.BORDER);
		comboLanguage.setBounds(120, 158, 219, 29);
		for (int i = 0; i < langs.length; i++) {
			comboLanguage.add(langs[i]);
		}

		buttons = new CrudButtons(composite, prp);
		buttons.setLocation(29, 214);
		buttons.setEnableDelete(Boolean.FALSE);
		setCrudActions(buttons);
		addSaveModifyListenerToWidgets(buttons, new Widget[] { txEmail });

	}

	public void initScreen() {
		if (user != null) {
			txUsername.setText(user.getUsername());
			if (user.getEmail() != null) {
				txEmail.setText(user.getEmail());
			}
			if (StringUtils.isNotEmpty(user.getLanguage())) {
				comboLanguage.setText(user.getLanguage());
			}
		}
	}

	public void clearAction() {
	}

	public boolean isEnabledSave() {
		return (txEmail.getText() != null && txEmail.getText().length() > 0);
	}

	public boolean isEnabledDelete() {
		return false;
	}

	public void saveAction() {
		User managedUser = userBusiness.getCurrentUser();
		managedUser.setEmail(txEmail.getText());
		managedUser.setLanguage(comboLanguage.getText());
		userBusiness.save(managedUser);
		setVisible(Boolean.FALSE);
	}

	public void deleteAction() {
	}
}
