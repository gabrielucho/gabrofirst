package br.com.gabro.first.screen.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import br.com.gabro.first.business.IPeriodBusiness;
import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.entity.Period;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.listener.MaskVerifyListener;
import br.com.gabro.first.screen.ITimeLauncherCrudScreen;
import br.com.gabro.first.screen.component.CrudButtons;
import br.com.gabro.first.screen.component.DateComposite;
import br.com.gabro.first.util.Props;
import br.com.gabro.first.util.TimeUtil;

@Named
public class PeriodScreen extends AbstractNoResizableScreen implements ITimeLauncherCrudScreen {

	@Inject
	private IPeriodBusiness periodBusiness;

	@Inject
	private IUserBusiness userBusiness;

	@Inject
	private Props prp;

	@Inject
	@Named("currentUser")
	private User user;

	private SimpleDateFormat sdf;

	private MaskVerifyListener timeListener;

	private DateComposite dateComposite;

	private Text txStartTime;
	private Text txEndTime;

	private Label lbActualTableDate;
	private Label lbDayChargeSum;
	private Table tbPeriods;

	private Button btStartNow;
	private Button btEndNow;
	private CrudButtons buttons;

	private Period oldPeriod;

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public PeriodScreen() {
		super();
		setSize(new Point(428, 370));
		prp = new Props();
		super.composeWindowBase();

		setText(prp.get("timelog.title"));
		lblScreenTitle.setText(prp.get("timelog.title"));

		sdf = new SimpleDateFormat(prp.get("mask.date"));

		dateComposite = new DateComposite(composite, prp);
		dateComposite.setLocation(14, 52);

		timeListener = new MaskVerifyListener(prp.get("mask.time"));

		Label lbEntrada = new Label(composite, SWT.NONE);
		lbEntrada.setBounds(17, 104, 81, 18);
		lbEntrada.setText(prp.get("timelog.starttime"));

		Label lbSaida = new Label(composite, SWT.NONE);
		lbSaida.setBounds(224, 105, 79, 16);
		lbSaida.setText(prp.get("timelog.endtime"));

		txStartTime = new Text(composite, SWT.BORDER);
		txStartTime.setBounds(102, 98, 64, 26);

		txStartTime.addVerifyListener(timeListener);
		// txStartTime.addModifyListener(saveModify);

		btStartNow = new Button(composite, SWT.NONE);
		btStartNow.setFont(new Font(this.getDisplay(), AbstractNoResizableScreen.MAIN_FONT_FAMILY, 9, SWT.NORMAL));
		btStartNow.setBounds(172, 97, 29, 28);
		btStartNow.setText("c");
		addListenerToStartNowButton();

		txEndTime = new Text(composite, SWT.BORDER);
		txEndTime.setBounds(303, 98, 64, 26);
		txEndTime.addVerifyListener(timeListener);
		// txEndTime.addModifyListener(saveModify);

		btEndNow = new Button(composite, SWT.NONE);
		btEndNow.setBounds(373, 97, 29, 28);
		btEndNow.setText("c");
		addListenerToEndNowButton();

		Label lbSpacer = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		lbSpacer.setBounds(10, 182, 399, 2);

		Label lbDateTable = new Label(composite, SWT.RIGHT);
		lbDateTable.setBounds(10, 191, 107, 16);
		lbDateTable.setText(prp.get("timelog.day.periods"));

		lbActualTableDate = new Label(composite, SWT.NONE);
		lbActualTableDate.setBounds(123, 191, 85, 16);

		Label lbDayCharge = new Label(composite, SWT.RIGHT);
		lbDayCharge.setBounds(238, 191, 92, 16);
		lbDayCharge.setText(prp.get("timelog.total.of.day"));

		lbDayChargeSum = new Label(composite, SWT.NONE);
		lbDayChargeSum.setBounds(342, 191, 48, 16);

		tbPeriods = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		tbPeriods.setBounds(9, 216, 400, 110);
		tbPeriods.setHeaderVisible(true);
		tbPeriods.setLinesVisible(true);
		tbPeriods.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				TableItem tbItemSelected = tbPeriods.getSelection()[0];
				oldPeriod = (Period) tbItemSelected.getData();
				txStartTime.removeVerifyListener(timeListener);
				txStartTime.setText(TimeUtil.extractTimeToDateTime(oldPeriod.getStartTime()));
				txStartTime.addVerifyListener(timeListener);
				txEndTime.removeVerifyListener(timeListener);
				if (oldPeriod.getEndTime() != null) {
					txEndTime.setText(TimeUtil.extractTimeToDateTime(oldPeriod.getEndTime()));
				} else {
					txEndTime.setText("");
				}
				txEndTime.addVerifyListener(timeListener);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
			}
		});

		TableColumn tbColNumber = new TableColumn(tbPeriods, SWT.NONE);
		tbColNumber.setWidth(48);
		tbColNumber.setText("#");

		TableColumn tbColStartHour = new TableColumn(tbPeriods, SWT.NONE);
		tbColStartHour.setWidth(100);
		tbColStartHour.setText(prp.get("timelog.starttime"));

		TableColumn tbColEndHour = new TableColumn(tbPeriods, SWT.NONE);
		tbColEndHour.setWidth(100);
		tbColEndHour.setText(prp.get("timelog.endtime"));

		TableColumn tbColPartTime = new TableColumn(tbPeriods, SWT.NONE);
		tbColPartTime.setWidth(100);
		tbColPartTime.setText(prp.get("timelog.chargehour"));

		buttons = new CrudButtons(composite, prp);
		buttons.setBounds(92, 137, 310, 32);
		buttons.setEnableDelete(Boolean.FALSE);
		setCrudActions(buttons);

		dateComposite.addModifyListenerToTxDate(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				if (dateComposite.getDateText().length() == 10) {
					// sdf.applyPattern(prp.get("mask.date"));
					try {
						loadPeriodsByDate(dateComposite.getDateText());
						lbActualTableDate.setText(dateComposite.getDateText());
					} catch (Exception e) {
						TimeUtil.handleError(e);
					}
				}
			}
		});

		lbActualTableDate.setText(dateComposite.getDateText());

		addSaveModifyListenerToWidgets(buttons, new Widget[] { txStartTime, txEndTime });
		addDeleteSelectionListenerToWidgets(buttons, new Widget[] { tbPeriods });
	}

	public void initScreen() {
		oldPeriod = null;
		if (dateComposite.getDateText() != null) {
			try {
				loadPeriodsByDate(dateComposite.getDateText());
			} catch (Exception e) {
				TimeUtil.handleError(e);
			}
		}
	}

	public void deleteAction() {
		if (oldPeriod != null) {
			Period periodToDelete = periodBusiness.getById(Period.class, oldPeriod.getId());
			periodBusiness.delete(periodToDelete);
			clearAction();
			initScreen();
		}
	}

	private void addListenerToStartNowButton() {
		btStartNow.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				txStartTime.removeVerifyListener(timeListener);
				txStartTime.setText(TimeUtil.getActualTime());
				txStartTime.addVerifyListener(timeListener);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				txStartTime.removeVerifyListener(timeListener);
				txStartTime.setText(TimeUtil.getActualTime());
				txStartTime.addVerifyListener(timeListener);
			}
		});
	}

	private void addListenerToEndNowButton() {
		btEndNow.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				txEndTime.removeVerifyListener(timeListener);
				txEndTime.setText(TimeUtil.getActualTime());
				txEndTime.addVerifyListener(timeListener);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				txEndTime.removeVerifyListener(timeListener);
				txEndTime.setText(TimeUtil.getActualTime());
				txEndTime.addVerifyListener(timeListener);
			}
		});
	}

	private void loadPeriodsByDate(String dateContent) {
		try {
			oldPeriod = null;
			int totalMinutes = 0;
			tbPeriods.removeAll();
			List<Period> periods = periodBusiness.getPeriodsByDate(user, TimeUtil.stringToDate(dateContent, prp.get("mask.date")));
			Integer counter = 1;
			for (Period p : periods) {
				TableItem tbItem = new TableItem(tbPeriods, SWT.NONE);
				String startTime = TimeUtil.extractTimeToDateTime(p.getStartTime());
				String endTime = TimeUtil.extractTimeToDateTime(p.getEndTime());
				String intervalTime = TimeUtil.getLabelOfElapsedTime(TimeUtil.getHoursAndMinutesBetweenDates(p.getEndTime(), p.getStartTime()));
				tbItem.setText(new String[] { Integer.toString(counter), startTime, endTime, intervalTime });
				tbItem.setData(p);
				if (p.getTotalMinutes() != 0) {
					totalMinutes += p.getTotalMinutes();
				} else {
					totalMinutes += TimeUtil.getElapsedMinutesBetweenDates(p.getEndTime(), p.getStartTime());
				}
				counter++;
			}
			Integer[] hoursAndMinutes = TimeUtil.calculateHoursAndMinutesFromTotalMinutes(totalMinutes);
			String hoursAndMinutesLabel = TimeUtil.getLabelOfElapsedTime(hoursAndMinutes);
			lbDayChargeSum.setText(hoursAndMinutesLabel);
		} catch (Exception e) {
			TimeUtil.handleError(e);
		}
	}

	public void clearAction() {
		// dateComposite.resetComponent();
		txStartTime.removeVerifyListener(timeListener);
		txStartTime.setText("");
		txStartTime.addVerifyListener(timeListener);
		txEndTime.removeVerifyListener(timeListener);
		txEndTime.setText("");
		txEndTime.addVerifyListener(timeListener);
		oldPeriod = null;
		tbPeriods.removeAll();
		lbActualTableDate.setText("-");
		lbDayChargeSum.setText(TimeUtil.HH_MM);
		loadPeriodsByDate(dateComposite.getDateText());
	}

	public Composite getComposite() {
		return composite;
	}

	public boolean isEnabledSave() {
		if (dateComposite.getDateText().length() == 10 && (txStartTime.getText() != null && txStartTime.getText().length() == 5)) {
			// && (txEndTime.getText() != null && txEndTime.getText().length()
			// == 5))
			// {
			return true;
		}
		return false;
	}

	public void saveAction() {
		Date date;
		sdf.applyPattern(prp.get("mask.date"));
		try {
			date = sdf.parse(dateComposite.getDateText());
		} catch (ParseException e) {
			date = Calendar.getInstance().getTime();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		Period period;
		if (oldPeriod != null) {
			period = periodBusiness.getById(Period.class, oldPeriod.getId());
		} else {
			period = new Period();
		}
		period.setDate(cal.getTime());
		String[] startTime = txStartTime.getText().split(":");
		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(startTime[0]));
		cal.set(Calendar.MINUTE, Integer.parseInt(startTime[1]));
		period.setStartTime(cal.getTime());
		if (txEndTime.getText().length() > 0) {
			String[] endTime = txEndTime.getText().split(":");
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(endTime[0]));
			cal.set(Calendar.MINUTE, Integer.parseInt(endTime[1]));
			period.setEndTime(cal.getTime());
		} else {
			period.setEndTime(null);
		}
		user = userBusiness.getCurrentUser();
		period.setUser(user);
		periodBusiness.save(period);
		clearAction();
		initScreen();
	}

	public boolean isEnabledDelete() {
		return (oldPeriod != null);
	}

}
