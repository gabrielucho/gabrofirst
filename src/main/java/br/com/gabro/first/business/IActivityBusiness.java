/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.business;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import br.com.gabro.first.entity.Activity;
import br.com.gabro.first.entity.Project;
import br.com.gabro.first.entity.User;

/**
 * @author leopoldo.barreiro
 * @since 20/11/2012 23:33:41
 */
@ApplicationScoped
public interface IActivityBusiness extends ITimeLauncherBusiness<Activity> {

  List<Activity> listAllByProject(Project project, User user);
  
  List<Activity> listAllActivesByProject(Project project, User user);
  
  List<Activity> listAllActives(User user);
  
}
