package br.com.gabro.first.producer;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@ApplicationScoped
public class EntityManagerProducer {

	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;

	public EntityManagerProducer() {
		entityManagerFactory = Persistence.createEntityManagerFactory("rdigitPersistenceUnit");
		entityManager = entityManagerFactory.createEntityManager();
	}

	@Produces
	@ApplicationScoped
	public EntityManager getEntityManager() {
		return entityManager;
	}

}
