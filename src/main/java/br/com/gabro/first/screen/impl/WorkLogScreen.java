/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.screen.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.wb.swt.SWTResourceManager;

import br.com.gabro.first.business.IActivityBusiness;
import br.com.gabro.first.business.IPeriodBusiness;
import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.business.IWorklogBusiness;
import br.com.gabro.first.entity.Activity;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.entity.Worklog;
import br.com.gabro.first.listener.MaskVerifyListener;
import br.com.gabro.first.screen.ITimeLauncherCrudScreen;
import br.com.gabro.first.screen.component.CrudButtons;
import br.com.gabro.first.screen.component.DateComposite;
import br.com.gabro.first.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 17/11/2012 17:41:12
 */
@Named
public class WorkLogScreen extends AbstractNoResizableScreen implements ITimeLauncherCrudScreen {

  @Inject
  @Named("defaultDisplay")
  private static Display display;

  @Inject
  private IWorklogBusiness business;

  @Inject
  private IPeriodBusiness periodBusiness;

  @Inject
  private IActivityBusiness activityBusiness;

  @Inject
  private IUserBusiness userBusiness;

  @Inject
  @Named("currentUser")
  private User user;

  private Worklog oldWorklog;
  private MaskVerifyListener timeListener;

  private DateComposite dateComposite;

  private Label lbActivity;
  private Combo cmbActivity;

  private Label lbTempo;
  private Text txTempo;
  private Button btSaldo;

  private Label lbDescricao;
  private Text txDescricao;
  private Table tbWorklogs;
  private TableColumn tbClItem;
  private TableColumn tbClActivity;
  private TableColumn tbClTempo;

  private Label lbTimelog;
  private Label lbTotalActivities;

  private List<Activity> activities;

  private CrudButtons buttons;

  /**
   * @param display
   * @param buzz
   * @param periodBuzz
   * @param usr
   */
  public WorkLogScreen() {
	super();
	setSize(new Point(362, 460));
	oldWorklog = null;
	activities = new ArrayList<Activity>(0);
	
	super.composeWindowBase();
	timeListener = new MaskVerifyListener(prp.get("mask.time"));

	dateComposite = new DateComposite(composite, prp);
	dateComposite.setLocation(11, 64);

	setText(prp.get("worklog.title"));
	lblScreenTitle.setText(prp.get("worklog.title"));

	lbActivity = new Label(composite, SWT.NONE);
	lbActivity.setBounds(14, 116, 74, 16);
	lbActivity.setText(prp.get("worklog.activity"));

	cmbActivity = new Combo(composite, SWT.NONE);
	cmbActivity.setBounds(96, 111, 246, 26);
	// cbActivity.addModifyListener(saveModify);

	lbTempo = new Label(composite, SWT.NONE);
	lbTempo.setBounds(15, 152, 63, 16);
	lbTempo.setText(prp.get("worklog.time"));

	txTempo = new Text(composite, SWT.BORDER);
	txTempo.setToolTipText(prp.get("worklog.hhmm"));
	txTempo.setBounds(96, 146, 63, 26);
	txTempo.addVerifyListener(timeListener);
	// txTempo.addModifyListener(saveModify);

	lbDescricao = new Label(composite, SWT.NONE);
	lbDescricao.setBounds(15, 183, 63, 16);
	lbDescricao.setText(prp.get("worklog.description"));

	txDescricao = new Text(composite, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.SCROLL_PAGE);
	txDescricao.setBounds(96, 183, 246, 51);
	// txDescricao.addModifyListener(saveModify);

	buttons = new CrudButtons(composite, prp);
	buttons.setLocation(32, 247);
	setCrudActions(buttons);

	tbWorklogs = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
	tbWorklogs.setBounds(11, 317, 331, 100);
	tbWorklogs.setHeaderVisible(true);
	tbWorklogs.setLinesVisible(true);

	tbClItem = new TableColumn(tbWorklogs, SWT.NONE);
	tbClItem.setWidth(40);
	tbClItem.setText("#");

	tbClActivity = new TableColumn(tbWorklogs, SWT.NONE);
	tbClActivity.setWidth(210);
	tbClActivity.setText(prp.get("worklog.activity"));

	tbClTempo = new TableColumn(tbWorklogs, SWT.NONE);
	tbClTempo.setWidth(69);
	tbClTempo.setText(prp.get("worklog.time"));

	Label lbSep1 = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
	lbSep1.setBounds(11, 285, 331, 2);
	

	Label lbInfoTimelog = new Label(composite, SWT.NONE);
	lbInfoTimelog.setAlignment(SWT.RIGHT);
	lbInfoTimelog.setBounds(10, 295, 113, 16);
	lbInfoTimelog.setText(prp.get("worklog.recordedhours"));

	lbTimelog = new Label(composite, SWT.NONE);
	lbTimelog.setBounds(130, 296, 50, 16);
	lbTimelog.setText(prp.get("worklog.hhmm"));

	Label lbInfoActivities = new Label(composite, SWT.NONE);
	lbInfoActivities.setAlignment(SWT.RIGHT);
	lbInfoActivities.setBounds(188, 295, 104, 16);
	lbInfoActivities.setText(prp.get("worklog.totalactivities"));

	lbTotalActivities = new Label(composite, SWT.NONE);
	lbTotalActivities.setBounds(297, 295, 47, 16);
	lbTotalActivities.setText(prp.get("worklog.hhmm"));

	Label lbSep2 = new Label(composite, SWT.SEPARATOR | SWT.VERTICAL);
	lbSep2.setBounds(182, 285, 6, 26);

	btSaldo = new Button(composite, SWT.NONE);
	btSaldo.setFont(SWTResourceManager.getFont("Ubuntu", 9, SWT.NORMAL));
	btSaldo.setToolTipText(prp.get("worklog.balancehours"));
	btSaldo.setBounds(167, 144, 77, 28);
	btSaldo.setText(prp.get("worklog.fill"));

	buttons.setEnableDelete(Boolean.FALSE);

	tbWorklogs.addSelectionListener(new SelectionListener() {
	  public void widgetSelected(SelectionEvent e) {
		oldWorklog = (Worklog) tbWorklogs.getSelection()[0].getData();
		loadComboActivities();
		// txActivity.setText(oldWorklog.getActivity());
		txDescricao.setText(oldWorklog.getDescription());
		txTempo.removeVerifyListener(timeListener);
		Integer[] hoursAndMinutes = TimeUtil.calculateHoursAndMinutesFromTotalMinutes(oldWorklog.getTotalMinutes());
		txTempo.setText(TimeUtil.getLabelOfElapsedTime(hoursAndMinutes));
		txTempo.addVerifyListener(timeListener);
	  }

	  public void widgetDefaultSelected(SelectionEvent e) {
	  }
	});

	dateComposite.addModifyListenerToTxDate(new ModifyListener() {
	  public void modifyText(ModifyEvent arg0) {
		if (dateComposite.getDateText().length() == 10) {
		  try {
			loadWorklogsByDate(dateComposite.getDateText());
			// txActivity.setText("");
			txTempo.removeVerifyListener(timeListener);
			txTempo.setText("");
			txTempo.addVerifyListener(timeListener);
			txDescricao.setText("");
		  } catch (Exception e) {
			TimeUtil.handleError(e);
		  }
		}
	  }
	});

	addSaveModifyListenerToWidgets(buttons, new Widget[] { txTempo, txDescricao, cmbActivity });
	addDeleteSelectionListenerToWidgets(buttons, new Widget[] { tbWorklogs });

  }

  public void initScreen() {
	oldWorklog = null;
	dateComposite.resetComponent();
	activities = activityBusiness.listAllActives(user);
	loadComboActivities();
	loadWorklogsByDate(dateComposite.getDateText());
  }

  public void clearAction() {
	oldWorklog = null;
	// txActivity.setText("");
	txTempo.removeVerifyListener(timeListener);
	txTempo.setText("");
	txTempo.addVerifyListener(timeListener);
	txDescricao.setText("");
	loadWorklogsByDate(dateComposite.getDateText());
  }

  public boolean isEnabledSave() {
	// TODO: considerar cbActivity
	if (txTempo != null && txTempo.getText() != null && txTempo.getText().length() > 0 && txDescricao != null && txDescricao.getText() != null
		&& txDescricao.getText().length() > 0) {
	  return true;
	} else {
	  return false;
	}
  }

  public void saveAction() {
	Worklog worklog = new Worklog();
	worklog.setLogDate(TimeUtil.stringToDate(dateComposite.getDateText(), prp.get("mask.date")));
	Activity selectedActivity = activities.get(cmbActivity.getSelectionIndex());
	worklog.setActivity(selectedActivity);
	worklog.setLabelHoursMinutes(txTempo.getText());
	worklog.setDescription(txDescricao.getText());
	if (oldWorklog != null) {
	  worklog.setId(oldWorklog.getId());
	  worklog.setOpened(oldWorklog.isOpened());
	}
	user = userBusiness.getCurrentUser();
	worklog.setUser(user);
	business.save(worklog);
	clearAction();
  }

  public void deleteAction() {
	if (oldWorklog != null) {
	  Worklog worklogToDelete = business.getById(Worklog.class, oldWorklog.getId());
	  business.delete(worklogToDelete);
	  clearAction();
	}
  }

  public void loadWorklogsByDate(String dateContent) {
	try {
	  oldWorklog = null;
	  int totalMinutes = 0;
	  tbWorklogs.removeAll();
	  List<Worklog> worklogs = business.getWorklogsByDate(user, TimeUtil.stringToDate(dateContent, prp.get("mask.date")));
	  Integer counter = 1;
	  for (Worklog w : worklogs) {
		TableItem tbItem = new TableItem(tbWorklogs, SWT.NONE);
		Integer[] hoursAndMinutes = TimeUtil.calculateHoursAndMinutesFromTotalMinutes(w.getTotalMinutes());
		String labelHhMm = TimeUtil.getLabelOfElapsedTime(hoursAndMinutes);
		tbItem.setText(new String[] { Integer.toString(counter), w.getActivity().getName(), labelHhMm });
		tbItem.setData(w);
		totalMinutes += w.getTotalMinutes();
		counter++;
	  }
	  Integer[] hoursAndMinutes = TimeUtil.calculateHoursAndMinutesFromTotalMinutes(totalMinutes);
	  String hoursAndMinutesLabel = TimeUtil.getLabelOfElapsedTime(hoursAndMinutes);
	  lbTotalActivities.setText(hoursAndMinutesLabel);

	  if (periodBusiness != null) {
		Integer totalWorkedTime = periodBusiness.getTotalMinutesWorkedByDate(user, TimeUtil.stringToDate(dateContent, prp.get("mask.date")));
		Integer[] hoursMinutesWorked = TimeUtil.calculateHoursAndMinutesFromTotalMinutes(totalWorkedTime);
		lbTimelog.setText(TimeUtil.getLabelOfElapsedTime(hoursMinutesWorked));
	  }
	} catch (Exception e) {
	  TimeUtil.handleError(e);
	}
  }

  public void loadComboActivities() {
	String[] acts = new String[activities.size()];
	int selected = 0;
	int counter = 0;
	for (Activity ac : activities) {
	  acts[counter] = ac.getProject().getAcronym() + ": " + ac.getName();
	  if (oldWorklog != null && oldWorklog.getActivity().getName().equalsIgnoreCase(ac.getName())) {
		selected = counter;
	  }
	  counter++;
	}
	cmbActivity.setItems(acts);
	cmbActivity.select(selected);
  }

  /** Getters and Setters */

  public IPeriodBusiness getPeriodBusiness() {
	return periodBusiness;
  }

  public void setPeriodBusiness(IPeriodBusiness periodBusiness) {
	this.periodBusiness = periodBusiness;
  }

  public boolean isEnabledDelete() {
	return (oldWorklog != null);
  }
}
