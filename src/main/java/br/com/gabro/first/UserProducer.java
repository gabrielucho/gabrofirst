/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.gabro.first.business.IUserBusiness;
import br.com.gabro.first.entity.User;

/**
 * @author leopoldo.barreiro
 * @since 17/12/2012 13:43:19
 */
@ApplicationScoped
public class UserProducer {

  @Inject
  private IUserBusiness userBusiness;
  
  private User currentUser;
  
  /**
   * 
   */
  public UserProducer() {
	
  }
  
  @Named("currentUser")
  @Produces
  @ApplicationScoped
  public User getCurrentUser() {
	currentUser = userBusiness.getCurrentUser();
	return currentUser;
  }

}
