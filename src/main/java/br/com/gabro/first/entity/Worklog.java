package br.com.gabro.first.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "worklog")
public class Worklog implements IEntity {

	private static final long serialVersionUID = 515629759001184762L;

	@Id
	@Column(name = "worklog_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id")
	private User user;

	@Temporal(TemporalType.DATE)
	@Column(name = "log_date")
	private Date logDate;

	@Column(name = "total_minutes", length = 5)
	private Integer totalMinutes;

	@ManyToOne(targetEntity = Activity.class)
	@JoinColumn(name = "activity_id")
	private Activity activity;

	@Column(name = "description", length = 300, nullable = true)
	private String description;

	@Column(name = "opened")
	private boolean opened;

	@Transient
	private String labelHoursMinutes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public Integer getTotalMinutes() {
		return totalMinutes;
	}

	public void setTotalMinutes(Integer totalMinutes) {
		this.totalMinutes = totalMinutes;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isOpened() {
		return opened;
	}

	public void setOpened(boolean opened) {
		this.opened = opened;
	}

	public String getLabelHoursMinutes() {
		return labelHoursMinutes;
	}

	public void setLabelHoursMinutes(String labelHoursMinutes) {
		this.labelHoursMinutes = labelHoursMinutes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activity == null) ? 0 : activity.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((labelHoursMinutes == null) ? 0 : labelHoursMinutes.hashCode());
		result = prime * result + ((logDate == null) ? 0 : logDate.hashCode());
		result = prime * result + (opened ? 1231 : 1237);
		result = prime * result + ((totalMinutes == null) ? 0 : totalMinutes.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Worklog other = (Worklog) obj;
		if (activity == null) {
			if (other.activity != null)
				return false;
		} else if (!activity.equals(other.activity))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (labelHoursMinutes == null) {
			if (other.labelHoursMinutes != null)
				return false;
		} else if (!labelHoursMinutes.equals(other.labelHoursMinutes))
			return false;
		if (logDate == null) {
			if (other.logDate != null)
				return false;
		} else if (!logDate.equals(other.logDate))
			return false;
		if (opened != other.opened)
			return false;
		if (totalMinutes == null) {
			if (other.totalMinutes != null)
				return false;
		} else if (!totalMinutes.equals(other.totalMinutes))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

}
