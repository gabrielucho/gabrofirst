package br.com.gabro.first.producer;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;

public class ResourceBundleProducer {

	public enum Languages {
		enUS("en"), esES("es"), ptBR("pt");

		private String lang;

		private Languages(String lang) {
			this.lang = lang;
		}

		public static Languages getLanguageByKey(String k) {
			if (StringUtils.isNotEmpty(k)) {
				if (k.equalsIgnoreCase("es")) {
					return Languages.esES;
				} else if (k.equalsIgnoreCase("pt")) {
					return Languages.ptBR;
				} else {
					return Languages.enUS;
				}
			} else {
				return Languages.enUS;
			}
		}
	}

	// @Produces
	// @ApplicationScoped
	// @Named("defaultResourceBundle")
	public ResourceBundle producesResourceBundle() {
		StringBuilder strb = new StringBuilder();
		strb.append(Languages.getLanguageByKey(Locale.getDefault().getLanguage()));
		strb.append(".properties");
		return ResourceBundle.getBundle(strb.toString());
	}
}
