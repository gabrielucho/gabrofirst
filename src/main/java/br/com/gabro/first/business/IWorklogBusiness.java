package br.com.gabro.first.business;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import br.com.gabro.first.entity.Activity;
import br.com.gabro.first.entity.User;
import br.com.gabro.first.entity.Worklog;

@ApplicationScoped
public interface IWorklogBusiness extends ITimeLauncherBusiness<Worklog> {

	/**
	 * Recupera uma lista de worklog por data, para o usuário indicado.
	 * 
	 * @param user
	 * @param date
	 * @return
	 */
	List<Worklog> getWorklogsByDate(User user, Date date);

	/**
	 * Realiza uma atualização no Worklog passado por parâmetro, adicionando ao
	 * tempo o intervalo indicado (em minutos)
	 * 
	 * @param idWorklog
	 * @param addInterval
	 */
	void updateChronometer(Long idWorklog, int addInterval);

	/**
	 * Recupera um Worklog aberto para o usuário informado
	 * 
	 * @param user
	 * @return
	 */
	Worklog getOpenedWorklog(User user);

	/**
	 * Permite iniciar um Worklog na data e hora atuais, na atividade indicada.
	 * Fecha todos os outros worklogs abertos para esse usuário.
	 * 
	 * @param activity
	 */
	void openWorklog(Activity activity);

	/**
	 * Pausa os worklogs do usuário que estiverem abertos.
	 * 
	 * @param user
	 */
	public void closeWorklogs(User user);

}
