package br.com.gabro.first.business;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Query;

import br.com.gabro.first.entity.User;
import br.com.gabro.first.util.TimeUtil;

@ApplicationScoped
public class UserBusiness extends AbstractTimeLauncherBusiness<User> implements IUserBusiness {

	public UserBusiness() {
		super();
	}

	public List<User> listAll(User user) {
		return null;
	}

	public User getCurrentUser() {
		String username = System.getProperty("user.name");
		Query query = entityManager.createQuery("SELECT u FROM User u WHERE u.username = :usernameSearch");
		query.setParameter("usernameSearch", username);
		User user = null;
		try {
			user = (User) query.getSingleResult();
		} catch (Exception e) {
			TimeUtil.handleError(e);
		}
		if (user == null) {
			user = new User();
			user.setUsername(username);
			save(user);
		}
		return user;
	}

}
