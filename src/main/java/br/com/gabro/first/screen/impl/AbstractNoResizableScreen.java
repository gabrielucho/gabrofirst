/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.gabro.first.screen.impl;

import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import br.com.gabro.first.screen.ITimeLauncherCrudScreen;
import br.com.gabro.first.screen.component.CrudButtons;
import br.com.gabro.first.screen.component.CrudButtons.ButtonType;
import br.com.gabro.first.util.Props;

/**
 * @author leopoldo.barreiro
 */
public abstract class AbstractNoResizableScreen extends Shell implements ITimeLauncherCrudScreen {

  public static final String MAIN_FONT_FAMILY = "Arial";
  public static final String TITLE_FONT = "Arial";
  public static final int TITLE_FONT_SIZE = 14;
  public static final int FIELD_FONT_SIZE = 10;
  public static final String WINDOW_LOGO = "/img/miniefaber.png";

  protected Composite composite;
  protected Label lblScreenTitle;
  protected Label lblLogotipo;
  protected Font fieldFont;

  protected Display display;

  @Inject
  protected Props prp;

  public AbstractNoResizableScreen() {
	super(Display.getDefault(), SWT.MIN | SWT.CLOSE | SWT.TITLE);
	display = Display.getDefault();
	prp = new Props();
	// setSize(windowSize);
	fieldFont = new Font(display, MAIN_FONT_FAMILY, FIELD_FONT_SIZE, SWT.NORMAL);

	composite = new Composite(this, SWT.BORDER);
	lblScreenTitle = new Label(composite, SWT.NONE);
	lblLogotipo = new Label(composite, SWT.NONE);
	lblScreenTitle.setFont(new Font(this.getDisplay(), TITLE_FONT, TITLE_FONT_SIZE, SWT.BOLD));

	Image imgLogo = new Image(this.getDisplay(), AbstractNoResizableScreen.class.getResourceAsStream(WINDOW_LOGO));
	lblLogotipo.setImage(imgLogo);

	addShellListener(new ShellListener() {

	  public void shellIconified(ShellEvent arg0) {
	  }

	  public void shellDeiconified(ShellEvent arg0) {
	  }

	  public void shellDeactivated(ShellEvent arg0) {
	  }

	  public void shellClosed(ShellEvent arg0) {
		arg0.doit = false;
		clearAction();
		setVisible(Boolean.FALSE);
	  }

	  public void shellActivated(ShellEvent arg0) {
	  }
	});

  }

  public void composeWindowBase() {
	int width = getSize().x;
	int height = getSize().y;

	composite.setBounds(4, 4, (width - 8), (height - 32));	
	lblScreenTitle.setBounds(20, 10, (width - 110), 30);
	lblLogotipo.setBounds((width - 80), 6, 50, 51);
	
	Monitor primary = display.getPrimaryMonitor();
	Rectangle bounds = primary.getBounds();
	Rectangle rect = getBounds();
	int x = bounds.x + (bounds.width - rect.width) / 2;
	int y = bounds.y + (bounds.height - rect.height) / 2;
	this.setLocation(x, y);
  }

  public void cancelAction() {
	clearAction();
	setVisible(Boolean.FALSE);
  }

  @Override
  protected void checkSubclass() {
	// Disable the check that prevents subclassing of SWT components
  }

  protected void setCrudActions(CrudButtons buttons) {
	buttons.addControl(ButtonType.SAVE, new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		saveAction();
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		saveAction();
	  }
	});
	buttons.addControl(ButtonType.CLEAR, new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		clearAction();
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		clearAction();
	  }
	});
	buttons.addControl(ButtonType.DELETE, new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		deleteAction();
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		deleteAction();
	  }
	});
	buttons.addControl(ButtonType.CLOSE, new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		cancelAction();
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		cancelAction();
	  }
	});
  }

  protected void addSaveModifyListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
	ModifyListener saveEnableModify = new ModifyListener() {
	  public void modifyText(ModifyEvent arg0) {
		buttons.setEnableSave(isEnabledSave());
	  }
	};
	for (int i = 0; i < widgets.length; i++) {
	  if (widgets[i].getClass().equals(Text.class)) {
		((Text) widgets[i]).addModifyListener(saveEnableModify);
	  } else if (widgets[i].getClass().equals(Combo.class)) {
		((Combo) widgets[i]).addModifyListener(saveEnableModify);
	  }
	}
  }

  protected void addSaveSelectionListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
	SelectionListener saveEnableSelection = new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		buttons.setEnableSave(isEnabledSave());
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		buttons.setEnableSave(isEnabledSave());
	  }
	};

	for (int i = 0; i < widgets.length; i++) {
	  if (widgets[i].getClass().equals(Button.class)) {
		((Button) widgets[i]).addSelectionListener(saveEnableSelection);
	  }
	}
  }

  protected void addDeleteModifyListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
	ModifyListener deleteEnableModify = new ModifyListener() {
	  public void modifyText(ModifyEvent arg0) {
		buttons.setEnableDelete(isEnabledSave());
	  }
	};
	for (int i = 0; i < widgets.length; i++) {
	  if (widgets[i].getClass().equals(Text.class)) {
		((Text) widgets[i]).addModifyListener(deleteEnableModify);
	  } else if (widgets[i].getClass().equals(Combo.class)) {
		((Combo) widgets[i]).addModifyListener(deleteEnableModify);
	  }
	}
  }

  protected void addDeleteSelectionListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
	SelectionListener deleteEnableSelection = new SelectionListener() {
	  public void widgetSelected(SelectionEvent arg0) {
		buttons.setEnableDelete(isEnabledDelete());
	  }

	  public void widgetDefaultSelected(SelectionEvent arg0) {
		buttons.setEnableDelete(isEnabledDelete());
	  }
	};

	for (int i = 0; i < widgets.length; i++) {
	  if (widgets[i].getClass().equals(Table.class)) {
		((Table) widgets[i]).addSelectionListener(deleteEnableSelection);
	  } else if (widgets[i].getClass().equals(Button.class)) {
		((Button) widgets[i]).addSelectionListener(deleteEnableSelection);
	  }
	}
  }

}
