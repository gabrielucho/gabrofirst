package br.com.gabro.first.screen.component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import br.com.gabro.first.listener.MaskVerifyListener;
import br.com.gabro.first.util.Props;
import br.com.gabro.first.util.TimeUtil;

public class DateComposite extends Composite {

	private Props prp;

	private SimpleDateFormat sdf;

	private Label lbDate;
	private Text txDate;
	private Label lbDayOfWeek;

	private Button btDayBefore;
	private Button btDayAfter;

	private Button btTypeToday;

	private MaskVerifyListener dateListener;

	public DateComposite(Composite composite) {
		super(composite, SWT.NONE);
		this.prp = new Props();
		initComponent();
		resetComponent();
	}

	public DateComposite(Composite composite, Props prp) {
		super(composite, SWT.NONE);
		this.prp = prp;
		initComponent();
		resetComponent();
	}

	private void initComponent() {
		setSize(330, 40);
		sdf = new SimpleDateFormat();
		// buttonFont = new Font(Display.getDefault(),
		// AbstractModalScreen.MAIN_FONT_FAMILY, 10, SWT.NORMAL);

		dateListener = new MaskVerifyListener(prp.get("mask.date"));

		lbDate = new Label(this, SWT.NONE);
		lbDate.setBounds(4, 11, 80, 20);
		lbDate.setText(prp.get("date"));

		txDate = new Text(this, SWT.BORDER);
		txDate.setBounds(86, 5, 90, 30);
		txDate.addVerifyListener(dateListener);
		txDate.addModifyListener(new TxDateModifyListener());

		lbDayOfWeek = new Label(this, SWT.NONE);
		lbDayOfWeek.setBounds(180, 11, 34, 20);

		btDayBefore = new Button(this, SWT.NONE);
		btDayBefore.setBounds(220, 5, 30, 30);
		btDayBefore.setText(prp.get("button.yesterday"));
		btDayBefore.addSelectionListener(new DayBeforeSelectionListener());

		btDayAfter = new Button(this, SWT.NONE);
		btDayAfter.setBounds(254, 5, 30, 30);
		btDayAfter.setText(prp.get("button.tomorrow"));
		btDayAfter.addSelectionListener(new DayAfterSelectionListener());

		btTypeToday = new Button(this, SWT.NONE);
		btTypeToday.setBounds(290, 5, 40, 30);
		btTypeToday.setText(prp.get("today.acronym"));
		btTypeToday.addSelectionListener(new TodaySelectionListener());
	}

	public void resetComponent() {
		txDate.removeVerifyListener(dateListener);
		txDate.setText(TimeUtil.actualDateString(prp.get("mask.date")));
		txDate.addVerifyListener(dateListener);
		lbDayOfWeek.setText(TimeUtil.dayOfWeek(TimeUtil.stringToDate(txDate.getText(), prp.get("mask.date")), prp));
	}

	private class TxDateModifyListener implements ModifyListener {

		public TxDateModifyListener() {
		}

		public void modifyText(ModifyEvent arg0) {
			if (txDate.getText().length() == 10) {
				sdf.applyPattern(prp.get("mask.date"));
				try {
					lbDayOfWeek.setText(TimeUtil.dayOfWeek(sdf.parse(txDate.getText()), prp));
				} catch (Exception e) {
					TimeUtil.handleError(e);
				}
			} else {
				lbDayOfWeek.setText("?");
			}
		}
	}

	private class DayBeforeSelectionListener implements SelectionListener {

		public void widgetSelected(SelectionEvent arg0) {
			doAction();
		}

		public void widgetDefaultSelected(SelectionEvent arg0) {
			doAction();
		}

		public final void doAction() {
			if (txDate.getText().length() == 10) {
				Date dt = TimeUtil.stringToDate(txDate.getText(), prp.get("mask.date"));
				Date dtBefore = TimeUtil.oneDayBefore(dt);
				txDate.removeVerifyListener(dateListener);
				txDate.setText(TimeUtil.dateToString(dtBefore, prp.get("mask.date")));
				txDate.addVerifyListener(dateListener);
			}
		}
	}

	private class DayAfterSelectionListener implements SelectionListener {

		public void widgetDefaultSelected(SelectionEvent arg0) {
			doAction();
		}

		public void widgetSelected(SelectionEvent arg0) {
			doAction();
		}

		public final void doAction() {
			if (txDate.getText().length() == 10) {
				Date dt = TimeUtil.stringToDate(txDate.getText(), prp.get("mask.date"));
				Date dtAfter = TimeUtil.oneDayAfter(dt);
				txDate.removeVerifyListener(dateListener);
				txDate.setText(TimeUtil.dateToString(dtAfter, prp.get("mask.date")));
				txDate.addVerifyListener(dateListener);
			}
		}
	}

	private class TodaySelectionListener implements SelectionListener {

		public void widgetDefaultSelected(SelectionEvent arg0) {
			doAction();
		}

		public void widgetSelected(SelectionEvent arg0) {
			doAction();
		}

		private void doAction() {
			txDate.removeVerifyListener(dateListener);
			txDate.setText(TimeUtil.actualDateString(prp.get("mask.date")));
			txDate.addVerifyListener(dateListener);
		}
	}

	public void addModifyListenerToTxDate(ModifyListener modifyListener) {
		txDate.addModifyListener(modifyListener);
	}

	public void removeModifyListenerToTxDate(ModifyListener modifyListener) {
		txDate.removeModifyListener(modifyListener);
	}

	public String getDateText() {
		return this.txDate.getText();
	}

	public Date getDate() {
		Date dt = null;
		sdf.applyPattern(prp.get("mask.date"));
		try {
			dt = sdf.parse(txDate.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dt;
	}

}
